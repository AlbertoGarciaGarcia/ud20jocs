package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;
import javax.swing.JInternalFrame;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class views extends JFrame {
	//Inicio todas las variables que necessito
	private static int numPalabras;
	private static String palabra;
	private static char[] palabraPorLetra;
	private static int vidas = 5;
	private ImageIcon img1 = new ImageIcon("..//UD20Ahorcado//src//main//java//img1.PNG");
	private ImageIcon img2 = new ImageIcon("..//UD20Ahorcado//src//main//java//img2.PNG");
	private ImageIcon img3 = new ImageIcon("..//UD20Ahorcado//src//main//java//img3.PNG");
	private ImageIcon img4 = new ImageIcon("..//UD20Ahorcado//src//main//java//img4.PNG");
	private ImageIcon img5 = new ImageIcon("..//UD20Ahorcado//src//main//java//img5.PNG");
	static JLabel lblLetra1;
	static JLabel lblLetra2;
	static JLabel lblLetra3;
	static JLabel lblLetra4;
	static JLabel lblLetra5;
	static JLabel lblLetra6;
	static JLabel lblLetra7;
	static JLabel lblVida1;
	static JLabel lblVida2;
	static JLabel lblVida3;
	static JLabel lblVida4;
	static JLabel lblVida5;
	static JLabel lblImagenAhorcado1;
	static JLabel lblImagenAhorcado2;
	static JLabel lblImagenAhorcado3;
	static JLabel lblImagenAhorcado4;
	static JLabel lblImagenAhorcado5;
	static JToggleButton btnA;
	static JToggleButton btnB;
	static JToggleButton btnC;
	static JToggleButton btnD;
	static JToggleButton btnE;
	static JToggleButton btnF;
	static JToggleButton btnG;
	static JToggleButton btnH;
	static JToggleButton btnI;
	static JToggleButton btnJ;
	static JToggleButton btnK;
	static JToggleButton btnL;
	static JToggleButton btnM;
	static JToggleButton btnN;
	static JToggleButton btnÑ;
	static JToggleButton btnO;
	static JToggleButton btnP;
	static JToggleButton btnQ;
	static JToggleButton btnR;
	static JToggleButton btnS;
	static JToggleButton btnT;
	static JToggleButton btnU;
	static JToggleButton btnV;
	static JToggleButton btnW;
	static JToggleButton btnX;
	static JToggleButton btnY;
	static JToggleButton btnZ;
	static JButton btnResolver;
	
	public views() {
		getContentPane().setLayout(null);
		setTitle("Ahorcado");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 614, 553);
		setVisible(true);
		
		//Aqui pongo una etiqueta para el menu, la palabra secreta y el teclado
		JLabel etiquetaMenu = new JLabel("Menu");
		etiquetaMenu.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		etiquetaMenu.setBounds(26, 27, 46, 14);
		getContentPane().add(etiquetaMenu);
		
		JLabel etiquetaPalabraSecreta = new JLabel("Palabra Secreta");
		etiquetaPalabraSecreta.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		etiquetaPalabraSecreta.setBounds(26, 226, 115, 14);
		getContentPane().add(etiquetaPalabraSecreta);
		
		JLabel lblTeclado = new JLabel("Teclado");
		lblTeclado.setFont(new Font("Segoe UI", Font.PLAIN, 14));
		lblTeclado.setBounds(26, 286, 56, 14);
		getContentPane().add(lblTeclado);
		
		//Boton para iniciar el juego
		JButton BtnIniciarJuego = new JButton("Iniciar Juego");
		BtnIniciarJuego.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				iniciarJuego();
			}
		});
		BtnIniciarJuego.setBounds(26, 52, 219, 56);
		getContentPane().add(BtnIniciarJuego);
		
		btnResolver = new JButton("Resolver");
		btnResolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vidas--;
				restarVida();
				primeraPalabra();
				btnResolver.setEnabled(false);
				
			}
		});
		btnResolver.setBounds(26, 119, 219, 56);
		getContentPane().add(btnResolver);
		
		
		//----LETRAS PARA APRETAR----
		btnA = new JToggleButton("A");
		btnA.setBounds(26, 305, 46, 28);
		getContentPane().add(btnA);
		btnA.addActionListener(aprietanTecla);
		
		btnB = new JToggleButton("B");
		btnB.setBounds(74, 305, 46, 28);
		getContentPane().add(btnB);
		btnB.addActionListener(aprietanTecla);
		
		btnC = new JToggleButton("C");
		btnC.setBounds(123, 305, 46, 28);
		getContentPane().add(btnC);
		btnC.addActionListener(aprietanTecla);
		
		btnD = new JToggleButton("D");
		btnD.setBounds(173, 305, 46, 28);
		getContentPane().add(btnD);
		btnD.addActionListener(aprietanTecla);
		
		btnE = new JToggleButton("E");
		btnE.setBounds(223, 305, 46, 28);
		getContentPane().add(btnE);
		btnE.addActionListener(aprietanTecla);
		
		btnF = new JToggleButton("F");
		btnF.setBounds(26, 335, 46, 28);
		getContentPane().add(btnF);
		btnF.addActionListener(aprietanTecla);
		
		btnG = new JToggleButton("G");
		btnG.setBounds(74, 335, 46, 28);
		getContentPane().add(btnG);
		btnG.addActionListener(aprietanTecla);
		
		btnH = new JToggleButton("H");
		btnH.setBounds(123, 335, 46, 28);
		getContentPane().add(btnH);
		btnH.addActionListener(aprietanTecla);
		
		btnI = new JToggleButton("I");
		btnI.setBounds(173, 335, 46, 28);
		getContentPane().add(btnI);
		btnI.addActionListener(aprietanTecla);
		
		btnJ = new JToggleButton("J");
		btnJ.setBounds(223, 335, 46, 28);
		getContentPane().add(btnJ);
		btnJ.addActionListener(aprietanTecla);
		
		btnK = new JToggleButton("K");
		btnK.setBounds(26, 366, 46, 28);
		getContentPane().add(btnK);
		btnK.addActionListener(aprietanTecla);
		
		btnL = new JToggleButton("L");
		btnL.setBounds(74, 366, 46, 28);
		getContentPane().add(btnL);
		btnL.addActionListener(aprietanTecla);
		
		btnM = new JToggleButton("M");
		btnM.setBounds(123, 366, 46, 28);
		getContentPane().add(btnM);
		btnM.addActionListener(aprietanTecla);
		
		btnN = new JToggleButton("N");
		btnN.setBounds(173, 366, 46, 28);
		getContentPane().add(btnN);
		btnN.addActionListener(aprietanTecla);
		
		btnÑ = new JToggleButton("Ñ");
		btnÑ.setBounds(223, 366, 46, 28);
		getContentPane().add(btnÑ);
		btnÑ.addActionListener(aprietanTecla);
		
		btnO = new JToggleButton("O");
		btnO.setBounds(26, 398, 46, 28);
		getContentPane().add(btnO);
		btnO.addActionListener(aprietanTecla);
		
		btnP = new JToggleButton("P");
		btnP.setBounds(74, 398, 46, 28);
		getContentPane().add(btnP);
		btnP.addActionListener(aprietanTecla);
		
		btnQ = new JToggleButton("Q");
		btnQ.setBounds(123, 398, 46, 28);
		getContentPane().add(btnQ);
		btnQ.addActionListener(aprietanTecla);
		
		btnR = new JToggleButton("R");
		btnR.setBounds(173, 398, 46, 28);
		getContentPane().add(btnR);
		btnR.addActionListener(aprietanTecla);
		
		btnS = new JToggleButton("S");
		btnS.setBounds(223, 398, 46, 28);
		getContentPane().add(btnS);
		btnS.addActionListener(aprietanTecla);
		
		btnT = new JToggleButton("T");
		btnT.setBounds(26, 429, 46, 28);
		getContentPane().add(btnT);
		btnT.addActionListener(aprietanTecla);
		
		btnU = new JToggleButton("U");
		btnU.setBounds(74, 429, 46, 28);
		getContentPane().add(btnU);
		btnU.addActionListener(aprietanTecla);
		
		btnV = new JToggleButton("V");
		btnV.setBounds(123, 429, 46, 28);
		getContentPane().add(btnV);
		btnV.addActionListener(aprietanTecla);
		
		btnW = new JToggleButton("w");
		btnW.setBounds(173, 429, 46, 28);
		getContentPane().add(btnW);
		btnW.addActionListener(aprietanTecla);
		
		btnX = new JToggleButton("X");
		btnX.setBounds(223, 429, 46, 28);
		getContentPane().add(btnX);
		btnX.addActionListener(aprietanTecla);
		
		btnY = new JToggleButton("Y");
		btnY.setBounds(26, 460, 46, 28);
		getContentPane().add(btnY);
		btnY.addActionListener(aprietanTecla);
		
		btnZ = new JToggleButton("Z");
		btnZ.setBounds(74, 460, 46, 28);
		getContentPane().add(btnZ);
		btnZ.addActionListener(aprietanTecla);
		
		
		//----label de cada letra de la palabra----
		lblLetra1 = new JLabel("-");
		lblLetra1.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra1.setBounds(26, 251, 28, 28);
		getContentPane().add(lblLetra1);
		
		lblLetra2 = new JLabel("-");
		lblLetra2.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra2.setBounds(60, 251, 28, 28);
		getContentPane().add(lblLetra2);
		
		lblLetra3 = new JLabel("-");
		lblLetra3.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra3.setBounds(94, 251, 28, 28);
		getContentPane().add(lblLetra3);
		
		lblLetra4 = new JLabel("-");
		lblLetra4.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra4.setBounds(128, 251, 28, 28);
		getContentPane().add(lblLetra4);
		
		lblLetra5 = new JLabel("-");
		lblLetra5.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra5.setBounds(162, 251, 28, 28);
		getContentPane().add(lblLetra5);
		
		lblLetra6 = new JLabel("-");
		lblLetra6.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra6.setBounds(196, 251, 28, 28);
		getContentPane().add(lblLetra6);
		
		lblLetra7 = new JLabel("-");
		lblLetra7.setHorizontalAlignment(SwingConstants.CENTER);
		lblLetra7.setBounds(230, 251, 28, 28);
		getContentPane().add(lblLetra7);
		
		
		//----label de los cuadrados de las vidas----
		lblVida1 = new JLabel("");
		lblVida1.setBackground(Color.RED);
		lblVida1.setOpaque(true);
		lblVida1.setBounds(26, 187, 28, 28);
		getContentPane().add(lblVida1);
		
		lblVida2 = new JLabel("");
		lblVida2.setBackground(Color.RED);
		lblVida2.setOpaque(true);
		lblVida2.setBounds(60, 187, 28, 28);
		getContentPane().add(lblVida2);
		
		lblVida3 = new JLabel("");
		lblVida3.setBackground(Color.RED);
		lblVida3.setOpaque(true);
		lblVida3.setBounds(94, 187, 28, 28);
		getContentPane().add(lblVida3);
		
		lblVida4 = new JLabel("");
		lblVida4.setBackground(Color.RED);
		lblVida4.setOpaque(true);
		lblVida4.setBounds(128, 187, 28, 28);
		getContentPane().add(lblVida4);
		
		lblVida5 = new JLabel("");
		lblVida5.setBackground(Color.RED);
		lblVida5.setOpaque(true);
		lblVida5.setBounds(162, 187, 28, 28);
		getContentPane().add(lblVida5);
		
		
		//----Labels de las imagenes del ahorcado----
		lblImagenAhorcado1 = new JLabel("");
		lblImagenAhorcado1.setIcon(img1);
		lblImagenAhorcado1.setVisible(false);
		lblImagenAhorcado1.setBounds(299, 40, 289, 463);
		getContentPane().add(lblImagenAhorcado1);
		
		lblImagenAhorcado2 = new JLabel("");
		lblImagenAhorcado2.setIcon(img2);
		lblImagenAhorcado2.setVisible(false);
		lblImagenAhorcado2.setBounds(299, 40, 289, 463);
		getContentPane().add(lblImagenAhorcado2);
		
		lblImagenAhorcado3 = new JLabel("");
		lblImagenAhorcado3.setIcon(img3);
		lblImagenAhorcado3.setVisible(false);
		lblImagenAhorcado3.setBounds(299, 40, 289, 463);
		getContentPane().add(lblImagenAhorcado3);
		
		lblImagenAhorcado4 = new JLabel("");
		lblImagenAhorcado4.setIcon(img4);
		lblImagenAhorcado4.setVisible(false);
		lblImagenAhorcado4.setBounds(299, 40, 289, 463);
		getContentPane().add(lblImagenAhorcado4);
		
		lblImagenAhorcado5 = new JLabel("");
		lblImagenAhorcado5.setIcon(img5);
		lblImagenAhorcado5.setVisible(false);
		lblImagenAhorcado5.setBounds(299, 40, 289, 463);
		getContentPane().add(lblImagenAhorcado5);
		
		preguntarPalabra();
	}
	
	//Cuando aprietan un boton lanza este metodo que hace las comprovaciones
	ActionListener aprietanTecla = new ActionListener() {
		public void actionPerformed(ActionEvent e) {
			e.getActionCommand();
			switch (e.getActionCommand()) {
			case "A":
				btnA.setEnabled(false);
				comprovarLetra('A');
				break;
			case "B":
				btnB.setEnabled(false);
				comprovarLetra('B');
				break;
			case "C":
				btnC.setEnabled(false);
				comprovarLetra('C');
				break;
			case "D":
				btnD.setEnabled(false);
				comprovarLetra('D');
				break;
			case "E":
				btnE.setEnabled(false);
				comprovarLetra('E');
				break;
			case "F":
				btnF.setEnabled(false);
				comprovarLetra('F');
				break;
			case "G":
				btnG.setEnabled(false);
				comprovarLetra('G');
				break;
			case "H":
				btnH.setEnabled(false);
				comprovarLetra('H');
				break;
			case "I":
				btnI.setEnabled(false);
				comprovarLetra('I');
				break;
			case "J":
				btnJ.setEnabled(false);
				comprovarLetra('J');
				break;
			case "K":
				btnK.setEnabled(false);
				comprovarLetra('K');
				break;
			case "L":
				btnL.setEnabled(false);
				comprovarLetra('L');
				break;
			case "M":
				btnM.setEnabled(false);
				comprovarLetra('M');
				break;
			case "N":
				btnN.setEnabled(false);
				comprovarLetra('N');
				break;
			case "Ñ":
				btnÑ.setEnabled(false);
				comprovarLetra('Ñ');
				break;
			case "O":
				btnO.setEnabled(false);
				comprovarLetra('O');
				break;
			case "P":
				btnP.setEnabled(false);
				comprovarLetra('P');
				break;
			case "Q":
				btnQ.setEnabled(false);
				comprovarLetra('Q');
				break;
			case "R":
				btnR.setEnabled(false);
				comprovarLetra('R');
				break;
			case "S":
				btnS.setEnabled(false);
				comprovarLetra('S');
				break;
			case "T":
				btnT.setEnabled(false);
				comprovarLetra('T');
				break;
			case "U":
				btnU.setEnabled(false);
				comprovarLetra('U');
				break;
			case "V":
				btnV.setEnabled(false);
				comprovarLetra('V');
				break;
			case "W":
				btnW.setEnabled(false);
				comprovarLetra('W');
				break;
			case "X":
				btnX.setEnabled(false);
				comprovarLetra('X');
				break;
			case "Y":
				btnY.setEnabled(false);
				comprovarLetra('Y');
				break;
			case "Z":
				btnZ.setEnabled(false);
				comprovarLetra('Z');
				break;
				
			}
			
		}
	};
	
	//Pregunto para iniciar el juego una palabra y compruevo que no sea mas grande que 7 que es el máximo de letras de mi juego
	public static void preguntarPalabra() {
		boolean buenaPalabra = false;
		palabra = " ";
		do {
			
			palabra = JOptionPane.showInputDialog(null, "Pon una palabra para iniciar el juego, de menos de 7 letras");
			
			if (palabra.length() <= 7) {
				buenaPalabra = true;
			}else {
				JOptionPane.showMessageDialog(null, "Pon una palabra de menos de 7 letras");
			}
			
		} while (!buenaPalabra);
		
		palabra = palabra.toUpperCase();
		
		palabraPorLetra = palabra.toCharArray();

		crearPalabra();

	}
	
	//Metodo para comprobar si la letra que pone esta en la palabra, y tambien mira  si has ganado
	public static void comprovarLetra(char letra) {
		int contador = 0;
		for (int i = 0; i < palabraPorLetra.length; i++) {
			if (palabraPorLetra[i] == letra) {
				if (i == 0) {
					lblLetra1.setText(String.valueOf(letra));
				}else if (i == 1) {
					lblLetra2.setText(String.valueOf(letra));
				}else if (i == 2) {
					lblLetra3.setText(String.valueOf(letra));
				}else if (i == 3) {
					lblLetra4.setText(String.valueOf(letra));
				}else if (i == 4) {
					lblLetra5.setText(String.valueOf(letra));
				}else if (i == 5) {
					lblLetra6.setText(String.valueOf(letra));
				}else if (i == 6) {
					lblLetra7.setText(String.valueOf(letra));
				}
			}else {
				contador++;
			}
		}
		//Aqui mira si no has acertado ninguna posición
		if (contador == palabraPorLetra.length) {
			vidas--;
			restarVida();
		}
		//Aqui mira si ganas
		if(vidas != 0) {
			if (numPalabras == 1) {
				if (lblLetra1.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 2) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 3) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-" && lblLetra3.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 4) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-" && lblLetra3.getText() != "-" && lblLetra4.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 5) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-" && lblLetra3.getText() != "-" && lblLetra4.getText() != "-" && lblLetra5.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 6) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-" && lblLetra3.getText() != "-" && lblLetra4.getText() != "-" && lblLetra5.getText() != "-" && lblLetra6.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}else if (numPalabras == 7) {
				if (lblLetra1.getText() != "-" && lblLetra2.getText() != "-" && lblLetra3.getText() != "-" && lblLetra4.getText() != "-" && lblLetra5.getText() != "-" && lblLetra6.getText() != "-" && lblLetra7.getText() != "-") {
					JOptionPane.showMessageDialog(null, "Has ganado");
					acabarJuego();
				}
			}
		}
		
	}
	
	//Un metodo para crear la palabra
	public static void crearPalabra() {
		numPalabras = palabraPorLetra.length;
		switch (palabraPorLetra.length) {
		case 1:
			lblLetra2.setVisible(false);
			lblLetra3.setVisible(false);
			lblLetra4.setVisible(false);
			lblLetra5.setVisible(false);
			lblLetra6.setVisible(false);
			lblLetra7.setVisible(false);
			break;
		case 2:
			lblLetra3.setVisible(false);
			lblLetra4.setVisible(false);
			lblLetra5.setVisible(false);
			lblLetra6.setVisible(false);
			lblLetra7.setVisible(false);
			break;
		case 3:
			lblLetra4.setVisible(false);
			lblLetra5.setVisible(false);
			lblLetra6.setVisible(false);
			lblLetra7.setVisible(false);
			break;
		case 4:
			lblLetra5.setVisible(false);
			lblLetra6.setVisible(false);
			lblLetra7.setVisible(false);
			break;
		case 5:
			lblLetra6.setVisible(false);
			lblLetra7.setVisible(false);
			break;
		case 6:
			lblLetra7.setVisible(false);
			break;
		case 7:
			break;
		
		}
	}
	
	//Para restar la vida, cambia la imagen y quita un cuadrado rojo
	public static void restarVida() {
		if (vidas == 4) {
			lblVida5.setVisible(false);
			lblImagenAhorcado1.setVisible(true);
		}else if (vidas == 3) {
			lblVida4.setVisible(false);
			lblImagenAhorcado2.setVisible(true);
			lblImagenAhorcado1.setVisible(false);
		}else if (vidas == 2) {
			lblVida3.setVisible(false);
			lblImagenAhorcado3.setVisible(true);
			lblImagenAhorcado2.setVisible(false);
		}else if (vidas == 1) {
			lblVida2.setVisible(false);
			lblImagenAhorcado4.setVisible(true);
			lblImagenAhorcado3.setVisible(false);
		}else if (vidas == 0) {
			lblVida1.setVisible(false);
			lblImagenAhorcado5.setVisible(true);
			lblImagenAhorcado4.setVisible(false);
			JOptionPane.showMessageDialog(null, "Has perdido :C");
			acabarJuego();
		}

	}
	
	//Para iniciar el juego, resetea todos los valores
	public static void iniciarJuego() {
		lblVida5.setVisible(true);
		lblVida4.setVisible(true);
		lblVida3.setVisible(true);
		lblVida2.setVisible(true);
		lblVida1.setVisible(true);
		lblLetra1.setText("-");
		lblLetra2.setText("-");
		lblLetra3.setText("-");
		lblLetra4.setText("-");
		lblLetra5.setText("-");
		lblLetra6.setText("-");
		lblLetra7.setText("-");
		lblLetra1.setVisible(true);
		lblLetra2.setVisible(true);
		lblLetra3.setVisible(true);
		lblLetra4.setVisible(true);
		lblLetra5.setVisible(true);
		lblLetra6.setVisible(true);
		lblLetra7.setVisible(true);
		lblLetra1.setText(String.valueOf("-"));
		lblLetra2.setText(String.valueOf("-"));
		lblLetra3.setText(String.valueOf("-"));
		lblLetra4.setText(String.valueOf("-"));
		lblLetra5.setText(String.valueOf("-"));
		lblLetra6.setText(String.valueOf("-"));
		lblLetra7.setText(String.valueOf("-"));
		lblImagenAhorcado1.setVisible(false);
		lblImagenAhorcado2.setVisible(false);
		lblImagenAhorcado3.setVisible(false);
		lblImagenAhorcado4.setVisible(false);
		lblImagenAhorcado5.setVisible(false);
		btnA.setEnabled(true);
		btnA.setSelected(false);
		btnB.setEnabled(true);
		btnB.setSelected(false);
		btnC.setEnabled(true);
		btnC.setSelected(false);
		btnD.setEnabled(true);
		btnD.setSelected(false);
		btnE.setEnabled(true);
		btnE.setSelected(false);
		btnF.setEnabled(true);
		btnF.setSelected(false);
		btnG.setEnabled(true);
		btnG.setSelected(false);
		btnH.setEnabled(true);
		btnH.setSelected(false);
		btnI.setEnabled(true);
		btnI.setSelected(false);
		btnJ.setEnabled(true);
		btnJ.setSelected(false);
		btnK.setEnabled(true);
		btnK.setSelected(false);
		btnL.setEnabled(true);
		btnL.setSelected(false);
		btnM.setEnabled(true);
		btnM.setSelected(false);
		btnN.setEnabled(true);
		btnN.setSelected(false);
		btnÑ.setEnabled(true);
		btnÑ.setSelected(false);
		btnO.setEnabled(true);
		btnO.setSelected(false);
		btnP.setEnabled(true);
		btnP.setSelected(false);
		btnQ.setEnabled(true);
		btnQ.setSelected(false);
		btnR.setEnabled(true);
		btnR.setSelected(false);
		btnS.setEnabled(true);
		btnS.setSelected(false);
		btnT.setEnabled(true);
		btnT.setSelected(false);
		btnU.setEnabled(true);
		btnU.setSelected(false);
		btnV.setEnabled(true);
		btnV.setSelected(false);
		btnW.setEnabled(true);
		btnW.setSelected(false);
		btnX.setEnabled(true);
		btnX.setSelected(false);
		btnY.setEnabled(true);
		btnY.setSelected(false);
		btnZ.setEnabled(true);
		btnZ.setSelected(false);
		btnResolver.setEnabled(true);
		numPalabras = 0;
		
		vidas = 5;
		preguntarPalabra();	
	}
	
	
	//Metodo de quando se acaba el juego
	public static void acabarJuego() {
		btnA.setEnabled(false);
		btnB.setEnabled(false);
		btnC.setEnabled(false);
		btnD.setEnabled(false);
		btnE.setEnabled(false);
		btnF.setEnabled(false);
		btnG.setEnabled(false);
		btnH.setEnabled(false);
		btnI.setEnabled(false);
		btnJ.setEnabled(false);
		btnK.setEnabled(false);
		btnL.setEnabled(false);
		btnM.setEnabled(false);
		btnN.setEnabled(false);
		btnÑ.setEnabled(false);
		btnO.setEnabled(false);
		btnP.setEnabled(false);
		btnQ.setEnabled(false);
		btnR.setEnabled(false);
		btnS.setEnabled(false);
		btnT.setEnabled(false);
		btnU.setEnabled(false);
		btnV.setEnabled(false);
		btnW.setEnabled(false);
		btnX.setEnabled(false);
		btnY.setEnabled(false);
		btnZ.setEnabled(false);
		btnResolver.setEnabled(false);
		
		if (numPalabras == 1) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
		}else if (numPalabras == 2) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
		}else if (numPalabras == 3) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
		}else if (numPalabras == 4) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
			lblLetra4.setText(String.valueOf(palabraPorLetra[3]));
		}else if (numPalabras == 5) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
			lblLetra4.setText(String.valueOf(palabraPorLetra[3]));
			lblLetra5.setText(String.valueOf(palabraPorLetra[4]));
		}else if (numPalabras == 6) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
			lblLetra4.setText(String.valueOf(palabraPorLetra[3]));
			lblLetra5.setText(String.valueOf(palabraPorLetra[4]));
			lblLetra6.setText(String.valueOf(palabraPorLetra[5]));
		}else if (numPalabras == 7) {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
			lblLetra4.setText(String.valueOf(palabraPorLetra[3]));
			lblLetra5.setText(String.valueOf(palabraPorLetra[4]));
			lblLetra6.setText(String.valueOf(palabraPorLetra[5]));
			lblLetra7.setText(String.valueOf(palabraPorLetra[6]));
		}
	}
	
	//Mirar primera palabra libre y enseñarla
	public static void primeraPalabra() {
		String letra = null;
		
		if (lblLetra1.getText() == "-") {
			lblLetra1.setText(String.valueOf(palabraPorLetra[0]));
			letra = String.valueOf(palabraPorLetra[0]);
		}else if (lblLetra2.getText() == "-") {
			lblLetra2.setText(String.valueOf(palabraPorLetra[1]));
			letra = String.valueOf(palabraPorLetra[1]);
		}else if (lblLetra3.getText() == "-") {
			lblLetra3.setText(String.valueOf(palabraPorLetra[2]));
			letra = String.valueOf(palabraPorLetra[2]);
		}else if (lblLetra4.getText() == "-") {
			lblLetra4.setText(String.valueOf(palabraPorLetra[3]));
			letra = String.valueOf(palabraPorLetra[3]);
		}else if (lblLetra5.getText() == "-") {
			lblLetra5.setText(String.valueOf(palabraPorLetra[4]));
			letra = String.valueOf(palabraPorLetra[4]);
		}else if (lblLetra6.getText() == "-") {
			lblLetra6.setText(String.valueOf(palabraPorLetra[5]));
			letra = String.valueOf(palabraPorLetra[5]);
		}else if (lblLetra7.getText() == "-") {
			lblLetra7.setText(String.valueOf(palabraPorLetra[6]));
			letra = String.valueOf(palabraPorLetra[6]);
		}
		comprovarLetra(letra.charAt(0));
		switch (letra) {
		case "A":
			btnA.setEnabled(false);
			break;
		case "B":
			btnB.setEnabled(false);
			break;
		case "C":
			btnC.setEnabled(false);
			break;
		case "D":
			btnD.setEnabled(false);
			break;
		case "E":
			btnE.setEnabled(false);
			break;
		case "F":
			btnF.setEnabled(false);
			break;
		case "G":
			btnG.setEnabled(false);
			break;
		case "H":
			btnH.setEnabled(false);
			break;
		case "I":
			btnI.setEnabled(false);
			break;
		case "J":
			btnJ.setEnabled(false);
			break;
		case "K":
			btnK.setEnabled(false);
			break;
		case "L":
			btnL.setEnabled(false);
			break;
		case "M":
			btnM.setEnabled(false);
			break;
		case "N":
			btnN.setEnabled(false);
			break;
		case "Ñ":
			btnÑ.setEnabled(false);
			break;
		case "O":
			btnO.setEnabled(false);
			break;
		case "P":
			btnP.setEnabled(false);
			break;
		case "Q":
			btnQ.setEnabled(false);
			break;
		case "R":
			btnR.setEnabled(false);
			break;
		case "S":
			btnS.setEnabled(false);
			break;
		case "T":
			btnT.setEnabled(false);
			break;
		case "U":
			btnU.setEnabled(false);
			break;
		case "V":
			btnV.setEnabled(false);
			break;
		case "W":
			btnW.setEnabled(false);
			break;
		case "X":
			btnX.setEnabled(false);
			break;
		case "Y":
			btnY.setEnabled(false);
			break;
		case "Z":
			btnZ.setEnabled(false);
			break;
			
		}
	}
}
