package vista;
import java.awt.AWTException;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.FlowLayout;
import java.awt.List;
import java.awt.Robot;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.lang.reflect.Array;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.awt.event.ActionEvent;
import java.util.Arrays;
public class VistaRaya extends JFrame {
// Delcaramos todas las variables
	private ImageIcon redonda = new ImageIcon("cruz.png");
	private ImageIcon cruz = new ImageIcon("redonda.png");
	private JPanel contentPane;
	private JTextField nombreJugador1;
	private JTextField nombreJugador2;
	private boolean turno = true;
	private int matrizPosiciones[][] = new int[3][3];
	private JLabel jugadorColoca = new JLabel(" coloca ficha....");
	private static JButton posicion1_1;
	private static JButton posicion1_2;
	private static JButton posicion1_3;
	private static JButton posicion2_1;
	private static JButton posicion2_2;
	private static JButton posicion2_3;
	private static JButton posicion3_1;
	private static JButton posicion3_2;
	private static JButton posicion3_3;
	private static int[] numeros = new int[] {0,1,2,3,4,5,6,7,8,9};
	

	public VistaRaya() {
		// Creamos tdodos los botones interfaces labels etc
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		jugadorColoca.setBounds(379, 44, 94, 14);
		contentPane.add(jugadorColoca);
		
		nombreJugador1 = new JTextField();
		nombreJugador1.setBounds(463, 92, 86, 20);
		contentPane.add(nombreJugador1);
		nombreJugador1.setColumns(10);
		
		JLabel nombre1 = new JLabel("Nombre : ");
		nombre1.setBounds(379, 95, 48, 14);
		contentPane.add(nombre1);
		
		JRadioButton humanoJugador1 = new JRadioButton("Humano");
		humanoJugador1.setBounds(437, 129, 86, 23);
		contentPane.add(humanoJugador1);
	
		
		JRadioButton cpuJugador1 = new JRadioButton("CPU");
		cpuJugador1.setBounds(523, 129, 109, 23);
		contentPane.add(cpuJugador1);
		
		ButtonGroup decisionJ1 = new ButtonGroup();
		decisionJ1.add(humanoJugador1);
		decisionJ1.add(cpuJugador1);
		JLabel lblNewLabel_2 = new JLabel("Tipo:  ");
		lblNewLabel_2.setBounds(379, 133, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("Jugador 2");
		lblNewLabel_3.setBounds(379, 187, 72, 14);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_1_1 = new JLabel("Nombre : ");
		lblNewLabel_1_1.setBounds(379, 225, 59, 14);
		contentPane.add(lblNewLabel_1_1);
		
		nombreJugador2 = new JTextField();
		nombreJugador2.setColumns(10);
		nombreJugador2.setBounds(463, 222, 86, 20);
		contentPane.add(nombreJugador2);
		
		JLabel lblNewLabel_2_1 = new JLabel("Tipo: ");
		lblNewLabel_2_1.setBounds(379, 261, 46, 14);
		contentPane.add(lblNewLabel_2_1);
		
		JRadioButton humanoJugador2 = new JRadioButton("Humano");
		humanoJugador2.setBounds(437, 257, 86, 23);
		contentPane.add(humanoJugador2);
		
		JRadioButton cpuJugador2 = new JRadioButton("CPU");
		cpuJugador2.setBounds(523, 257, 109, 23);
		contentPane.add(cpuJugador2);
		
		ButtonGroup decisionJ2 = new ButtonGroup();
		decisionJ2.add(humanoJugador2);
		decisionJ2.add(cpuJugador2);
		
		
		
		JLabel lblNewLabel = new JLabel("Jugador 1");
		lblNewLabel.setBounds(379, 69, 72, 14);
		contentPane.add(lblNewLabel);
		// Este proceso se repite para cada boton
		 posicion1_2 = new JButton(" ");
		posicion1_2.addActionListener(new ActionListener()
		{
		
			public void actionPerformed(ActionEvent e) {
		// Si el turno es verdadero, es decir del J1, al clicar se pondra en el turno del otro, y comprobara si su movimiento es el ganador
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion1_2.setIcon(cruz);
					matrizPosiciones[0][1]  = 1;
					if((matrizPosiciones[0][1] == 1 && matrizPosiciones[0][0] == 1 && matrizPosiciones[0][2] == 1) ||
					(matrizPosiciones[0][1] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[2][1] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					
					// Si esta seleccionada la opcion de cpu se movera segun la cpu
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					
					turno = false;
					posicion1_2.setEnabled(false);
					
					
					
				}
				
				// Y en caso de que sea del J2 sera al reves
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion1_2.setIcon(redonda);
					matrizPosiciones[0][1] = 2;
					if((matrizPosiciones[0][1] == 2 && matrizPosiciones[0][0]  == 2 && matrizPosiciones[0][2] == 2) ||
							(matrizPosiciones[0][1] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[2][1] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion1_2.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
				}
				
			}
		});
		posicion1_2.setBounds(117, 21, 100, 110);
		contentPane.add(posicion1_2);
		// Haremos lo mismo con todos los botones
		 posicion1_1 = new JButton(" ");
		posicion1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion1_1.setIcon(cruz);
					matrizPosiciones[0][0] = 1;
					
					
					
					if((matrizPosiciones[0][0] == 1 && matrizPosiciones[0][1] == 1 && matrizPosiciones[0][2] == 1) ||
					(matrizPosiciones[0][0] == 1 && matrizPosiciones[1][0] == 1 && matrizPosiciones[2][0] == 1) || 
					(matrizPosiciones[0][0] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[2][2] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					turno = false;
					posicion1_1.setEnabled(false);
			
					
				
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion1_1.setIcon(redonda);
					matrizPosiciones[0][0] = 2;
					if((matrizPosiciones[0][0] == 2 && matrizPosiciones[0][1] == 2 && matrizPosiciones[0][2] == 2) ||
							(matrizPosiciones[0][0] == 2 && matrizPosiciones[1][0] == 2 && matrizPosiciones[2][0] == 2) || 
							(matrizPosiciones[0][0] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[2][2] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
								
							}
					turno = true;
					posicion1_1.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
				
				}
				
			}
		});
		posicion1_1.setBounds(20, 21, 100, 110);
		contentPane.add(posicion1_1);
		
	posicion1_3 = new JButton(" ");
		posicion1_3.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion1_3.setIcon(cruz);
					turno = false;
					posicion1_3.setEnabled(false);
					
				
				
					matrizPosiciones[0][2] = 1;
					if((matrizPosiciones[0][0] == 1  && matrizPosiciones[0][1] == 1 && matrizPosiciones[0][2] == 1) ||
							(matrizPosiciones[0][2] == 1 && matrizPosiciones[1][2] == 1 && matrizPosiciones[2][2] == 1) || 
							(matrizPosiciones[0][2] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[2][0] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
							}
					
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion1_3.setIcon(redonda);
					matrizPosiciones[0][2] = 2;
					if((matrizPosiciones[0][0] == 2 && matrizPosiciones[0][1] == 2 && matrizPosiciones[0][2] == 2) ||
							(matrizPosiciones[0][2] == 2 && matrizPosiciones[1][2] == 2 && matrizPosiciones[2][2] == 2) || 
							(matrizPosiciones[0][2] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[2][0] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion1_3.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
					
				}
			}
		});
		posicion1_3.setBounds(213, 21, 100, 110);
		contentPane.add(posicion1_3);
		
	 posicion2_1 = new JButton(" ");
		posicion2_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion2_1.setIcon(cruz);
					matrizPosiciones[1][0]  = 1;
					if((matrizPosiciones[1][0] == 1 && matrizPosiciones[0][0] == 1 && matrizPosiciones[2][0] == 1) ||
					(matrizPosiciones[1][0] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[1][2] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					turno = false;
					posicion2_1.setEnabled(false);
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion2_1.setIcon(redonda);
					matrizPosiciones[1][0] = 2;
					if((matrizPosiciones[1][0] == 2 && matrizPosiciones[0][0]  == 2 && matrizPosiciones[2][0] == 2) ||
							(matrizPosiciones[1][0] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[1][2] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion2_1.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
					
				}
			}
		});
		
		posicion2_1.setBounds(20, 129, 100, 110);
		contentPane.add(posicion2_1);
		
	 posicion3_1 = new JButton(" ");
		posicion3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion3_1.setIcon(cruz);
					matrizPosiciones[2][0]  = 1;
					if((matrizPosiciones[2][0] == 1 && matrizPosiciones[2][1] == 1 && matrizPosiciones[2][2] == 1) ||
					(matrizPosiciones[2][0] == 1 && matrizPosiciones[1][0] == 1 && matrizPosiciones[0][0] == 1) ||
					 (matrizPosiciones[2][0] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[0][2] == 1)) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
				
					turno = false;
					posicion3_1.setEnabled(false);
					
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion3_1.setIcon(redonda);
					matrizPosiciones[2][0] = 2;
					if((matrizPosiciones[2][0] == 2 && matrizPosiciones[2][1]  == 2 && matrizPosiciones[2][2] == 2) ||
							(matrizPosiciones[2][0] == 2 && matrizPosiciones[1][0] == 2 && matrizPosiciones[0][0] == 2) ||
							(matrizPosiciones[2][0] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[0][2] == 2)) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion3_1.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
				
				}
			}
		});
		posicion3_1.setBounds(20, 232, 100, 110);
		contentPane.add(posicion3_1);
		
	 posicion2_2 = new JButton(" ");
		posicion2_2.addActionListener(new ActionListener() {
	
			public void actionPerformed(ActionEvent e) {
				
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion2_2.setIcon(cruz);
					matrizPosiciones[1][1]  = 1;
					if((matrizPosiciones[2][1] == 1 && matrizPosiciones[2][0] == 1 && matrizPosiciones[2][2] == 1) ||
					(matrizPosiciones[2][1] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[0][1] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					turno = false;
					posicion2_2.setEnabled(false);
					
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion2_2.setIcon(redonda);
					matrizPosiciones[1][1] = 2;
					if((matrizPosiciones[1][1] == 2 && matrizPosiciones[1][0]  == 2 && matrizPosiciones[1][2] == 2) ||
							(matrizPosiciones[2][1] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[0][1] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion2_2.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				
					
				}
			}
		});
		posicion2_2.setBounds(117, 129, 100, 110);
		contentPane.add(posicion2_2);
		
		 posicion3_2 = new JButton(" ");
		posicion3_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion3_2.setIcon(cruz);
					matrizPosiciones[2][1]  = 1;
					if((matrizPosiciones[2][1] == 1 && matrizPosiciones[2][0] == 1 && matrizPosiciones[2][2] == 1) ||
					(matrizPosiciones[2][1] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[0][1] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					turno = false;
					posicion3_2.setEnabled(false);
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion3_2.setIcon(redonda);
					matrizPosiciones[2][1] = 2;
					if((matrizPosiciones[2][1] == 2 && matrizPosiciones[2][0]  == 2 && matrizPosiciones[2][2] == 2) ||
							(matrizPosiciones[2][1] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[0][1] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion3_2.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				
				
				}
			}
		});
		posicion3_2.setBounds(117, 232, 100, 110);
		contentPane.add(posicion3_2);
		
	 posicion2_3 = new JButton(" ");
		posicion2_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion2_3.setIcon(cruz);
					matrizPosiciones[1][2]  = 1;
					if((matrizPosiciones[1][2] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[1][0] == 1) ||
					(matrizPosiciones[1][2] == 1 && matrizPosiciones[0][2] == 1 && matrizPosiciones[2][2] == 1) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					turno = false;
					posicion2_3.setEnabled(false);
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
			
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion2_3.setIcon(redonda);
					matrizPosiciones[1][2] = 2;
					if((matrizPosiciones[1][2] == 2 && matrizPosiciones[1][1]  == 2 && matrizPosiciones[1][0] == 2) ||
							(matrizPosiciones[1][2] == 2 && matrizPosiciones[0][2] == 2 && matrizPosiciones[2][2] == 2) ) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion2_3.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
				
					
				}
			}
		});
		posicion2_3.setBounds(213, 129, 100, 110);
		contentPane.add(posicion2_3);
		
		 posicion3_3 = new JButton(" ");
		posicion3_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(turno) {
					jugadorColoca.setText(nombreJugador1.getText() + " coloca...");
					posicion3_3.setIcon(cruz);
					matrizPosiciones[2][2]  = 1;
					if((matrizPosiciones[2][2] == 1 && matrizPosiciones[2][1] == 1 && matrizPosiciones[2][0] == 1) ||
					(matrizPosiciones[0][2] == 1 && matrizPosiciones[2][2] == 1 && matrizPosiciones[1][2] == 1)  ||
					(matrizPosiciones[2][2] == 1 && matrizPosiciones[1][1] == 1 && matrizPosiciones[0][0] == 1)){
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 1");
						
					}
					turno = false;
					posicion3_3.setEnabled(false);
					if(cpuJugador2.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
				}
				else {
					jugadorColoca.setText(nombreJugador2.getText() + " coloca...");
					posicion3_3.setIcon(redonda);
					matrizPosiciones[2][2] = 2;
					if((matrizPosiciones[2][2] == 2 && matrizPosiciones[2][1]  == 2 && matrizPosiciones[2][0] == 2) ||
							(matrizPosiciones[0][2] == 2 && matrizPosiciones[2][2] == 2 && matrizPosiciones[1][2] == 2) ||
							(matrizPosiciones[2][2] == 2 && matrizPosiciones[1][1] == 2 && matrizPosiciones[0][0] == 2)) {
						JOptionPane.showMessageDialog(null, "Ha ganado el jugador 2");
							}
					turno = true;
					posicion3_3.setEnabled(false);
					if(cpuJugador1.isSelected()) {
						try {
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
					}
					
				
				}
			}
		});
		posicion3_3.setBounds(213, 232, 100, 110);
		contentPane.add(posicion3_3);
		
	
		
	
		
		
		
		
		
		
		
		
		
		
		
	
		
		
		
		// Si le damos a nueva partida, resetearemos todo
		
		JButton nuevaPartida = new JButton("Nueva Partida");
		nuevaPartida.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				posicion3_3.setEnabled(true);
				posicion3_3.setIcon(null);
				posicion3_2.setEnabled(true);
				posicion3_2.setIcon(null);
				posicion3_1.setEnabled(true);
				posicion3_1.setIcon(null);
				posicion2_3.setEnabled(true);
				posicion2_3.setIcon(null);
				posicion2_2.setEnabled(true);
				posicion2_2.setIcon(null);
				posicion2_1.setEnabled(true);
				posicion2_1.setIcon(null);
				posicion1_1.setEnabled(true);
				posicion1_1.setIcon(null);
				posicion1_2.setEnabled(true);
				posicion1_2.setIcon(null);
				posicion1_3.setEnabled(true);
				posicion1_3.setIcon(null);
				matrizPosiciones[2][0]  = 0;
				matrizPosiciones[2][1]  = 0;
				matrizPosiciones[2][2]  = 0;
				matrizPosiciones[0][0]  = 0;
				matrizPosiciones[0][1]  = 0;
				matrizPosiciones[0][2]  = 0;
				matrizPosiciones[1][1]  = 0;
				matrizPosiciones[1][2]  = 0;
				matrizPosiciones[1][0]  = 0;
				
			// Simular partida de cpu en caso de que los dos esten seleccionados
				if(cpuJugador1.isSelected() && cpuJugador2.isSelected()) {
					for (int i = 0; i < 15;i++) {
						try {
							
							cpuJ1(matrizPosiciones);
						} catch (InterruptedException e1) {

							e1.printStackTrace();
						}	
					}
				}
			
				
				
				
					
					
			}
		});
		nuevaPartida.setBounds(420, 11, 129, 23);
		contentPane.add(nuevaPartida);
		
		
		
		
		
		
		
	
		setVisible(true);
		
		
		
		
		
		
		
	}
	
	

	
	
	

	
	// Usaremos la funcion de robot para simular los movimientos y los clicks del jugador
	public static void click(int x, int y) throws AWTException{
	    Robot bot = new Robot();
	   bot.delay(300);
	   bot.mouseMove(x, y);
	   bot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
       bot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	   bot.delay(300);
	 
	        
	   
	    
	}
	
	public static void cpuJ1(int matriz[][]) throws InterruptedException {
		int random = new Random().nextInt(numeros.length);
		boolean acierto = false;
		
		
			// Mediante esta funcion haremos que encuentre la posicion adecuada para hacer el click
			
				
		do {
			random = new Random().nextInt(numeros.length);
	
		if(random == 1 && matriz[0][0] == 0 && matriz[0][0] != 1 && matriz[0][0] != 2) {
			try {
				borrarArray(numeros,random);
				
				 
				
				click(posicion1_1.getBounds().x+20,posicion1_1.getBounds().y+35);
				
			
			} catch (AWTException e1) {
				e1.printStackTrace();
				
			}
			 acierto = true;
		}
		else {
			acierto = false;
		}
		if(random == 2 && matriz[0][1] == 0 && matriz[0][1] != 1 && matriz[0][1] != 2) {
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion1_2.getBounds().x+20,posicion1_2.getBounds().y+35);
				
				
			
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;	
		}
		
		else {
			acierto = false;
		}
		if(random == 3 && matriz[0][2] == 0 && matriz[0][2] != 1 && matriz[0][2] != 2) {
			try {
				borrarArray(numeros,random);
				 

				click(posicion1_3.getBounds().x+20,posicion1_3.getBounds().y+35);
				
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;	
		}
		else {
			acierto = false;
		}
		if(random == 4 && matriz[1][0] == 0 && matriz[1][0] != 1 && matriz[1][0] != 2 ) {
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion2_1.getBounds().x+20,posicion2_1.getBounds().y+35);
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;	
		}
		else {
			acierto = false;
		}
		if(random == 5 && matriz[1][1] == 0 && matriz[1][1] != 1 && matriz[1][1] != 2 ) {
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion2_2.getBounds().x+20,posicion2_2.getBounds().y+35);
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			
		}
		else {
			acierto = false;
		}
		
		if(random == 6 && matriz[1][2] == 0 && matriz[1][2] != 1 && matriz[1][2] != 2) {
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion2_3.getBounds().x+20,posicion2_3.getBounds().y+35);
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			
		}
		else {
			acierto = false;
		}
		if(random == 7 && matriz[2][0] == 0 && matriz[2][0] != 1 && matriz[2][0] != 2) {
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion3_1.getBounds().x+20,posicion3_1.getBounds().y+35);
				 
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;	
		}
		else {
			acierto = false;
		}
		if(random == 8 && matriz[2][1] == 0 && matriz[2][1] != 1 && matriz[2][1] != 2) {
			try {
				borrarArray(numeros,random);
				 
			
				click(posicion3_2.getBounds().x+20,posicion3_2.getBounds().y+35);
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;	
		}
		
		else {
			acierto = false;
		}
		
		if(random == 9 && matriz[2][2] == 0 && matriz[2][2] != 1 && matriz[2][2] != 2 ) {
			
			try {
				borrarArray(numeros,random);
				 
				
				click(posicion3_3.getBounds().x+20,posicion3_3.getBounds().y+35);
				
				
			} catch (AWTException e1) {
				e1.printStackTrace();
			}
			 acierto = true;
		}
		else {
			acierto = false;
		}
		}while(acierto = false);
		
		
		
		
		
		
	}
	
	
		
	 public static void borrarArray( int [] arr, int index ){
		    
		    int[] arrOut = new int[arr.length - 1];
		    int copiarRestantes = arr.length - ( index + 1 );
		    System.arraycopy(arr, 0, arrOut, 0, index);
		    System.arraycopy(arr, index + 1, arrOut, index, copiarRestantes);
		  }
	
	
	
	
	
	
	
	
	
	
	
	public static int numeroRandom() {
		int random = (int) (Math.random() * (9 - 1)) + 1;
		return random;
	}

	
}
