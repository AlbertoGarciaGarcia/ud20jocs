package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Selectornivel extends JFrame {

	private JPanel contentPane;
	
	private String nivel;

	public Selectornivel() {
		// Creo el panel
		setTitle("Seleccionar nivel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 364, 394);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setVisible(true);
		
		// Creo los radioButtons
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Principiante");
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nivel = "Principiante";
			}
		});
		rdbtnNewRadioButton.setBounds(115, 88, 97, 25);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnMedio = new JRadioButton("Medio");
		rdbtnMedio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nivel = "Medio";
			}
		});
		rdbtnMedio.setBounds(115, 138, 63, 25);
		contentPane.add(rdbtnMedio);
		
		JRadioButton rdbtnAvanzado = new JRadioButton("Avanzado");
		rdbtnAvanzado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				nivel = "Avanzado";
				
			}
		});
		rdbtnAvanzado.setBounds(115, 188, 90, 25);
		contentPane.add(rdbtnAvanzado);
		
		// Creo un button group y los añado
		ButtonGroup bgroup = new ButtonGroup();
		
		bgroup.add(rdbtnNewRadioButton);
		bgroup.add(rdbtnMedio);
		bgroup.add(rdbtnAvanzado);
		
		// Creo el boton de aceptar
		JButton btnNewButton = new JButton("Aceptar");
		// Le creo para que haga una acción
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (nivel.equals("Principiante")) {
					Principal principiante = new Principal();
					setVisible(false);
				}
				else if (nivel.equalsIgnoreCase("Medio")) {
					PrincipalMedio medio = new PrincipalMedio();
					setVisible(false);
				}
				else {
					PrincipalAvanzado avanzado = new PrincipalAvanzado();
					setVisible(false);
				}
			}
		});
		btnNewButton.setBounds(32, 257, 110, 34);
		contentPane.add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setBounds(183, 257, 110, 34);
		contentPane.add(btnCancelar);
		
	}
}
