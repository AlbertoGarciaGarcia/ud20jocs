package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

public class PrincipalAvanzado extends JFrame {

private JPanel contentPane;
	
	// ELEMENTOS CREADOS PARA MODIFICAR-----------------------------
	private Color colorBoton;

	Random rand = new Random();
	
	JButton btnResp1Sec1;
	JButton btnResp2Sec1;
	JButton btnResp3Sec1;
	JButton btnResp4Sec1;
	JButton btnResp1Sec2;
	JButton btnResp2Sec2;
	JButton btnResp3Sec2;
	JButton btnResp4Sec2;
	JButton btnResp1Sec3;
	JButton btnResp2Sec3;
	JButton btnResp3Sec3;
	JButton btnResp4Sec3;
	JButton btnResp1Sec4;
	JButton btnResp2Sec4;
	JButton btnResp3Sec4;
	JButton btnResp4Sec4;
	JButton btnResp1Sec5;
	JButton btnResp2Sec5;
	JButton btnResp3Sec5;
	JButton btnResp4Sec5;
	JButton btnResp1Sec6;
	JButton btnResp2Sec6;
	JButton btnResp3Sec6;
	JButton btnResp4Sec6;
	
	JButton btnComp2;
	JButton btnComp3;
	JButton btnComp4;
	JButton btnComp5;
	JButton btnComp6;
	
	JButton[] arrayBotones = new JButton[4];
	JButton[] arrayBotones2 = new JButton[4];
	JButton[] arrayBotones3 = new JButton[4];
	JButton[] arrayBotones4 = new JButton[4];
	JButton[] arrayBotones5 = new JButton[4];
	JButton[] arrayBotones6 = new JButton[4];
	JButton[] arrayBotones7 = new JButton[4];
	JButton[] arrayBotones8 = new JButton[4];
	
	JButton[] arrayBotonesSecuencia = new JButton[4];
	JButton[] arrayBotonesResp1 = new JButton[4];
	
	Color colorDisp1 = Color.BLUE;
	Color colorDisp2 = Color.GREEN;
	Color colorDisp3 = Color.MAGENTA;
	Color colorDisp4 = Color.ORANGE;
	Color colorDisp5 = Color.PINK;
	Color colorDisp6 = Color.RED;
	
	Color colorSec1;
	Color colorSec2;
	Color colorSec3;
	Color colorSec4;
	
	int contAzul;
	int contVerde;
	int contMagenta;
	int contNaranja;
	int contRosa;
	int contRojo;
	
	int contAzulGuess1;
	int contVerdeGuess1;
	int contMagentaGuess1;
	int contNaranjaGuess1;
	int contRosaGuess1;
	int contRojoGuess1;


	public PrincipalAvanzado() {
		
		//PANEL PRINCIPAL----------------------------
		setTitle("MasterMind");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 908, 499);
		setVisible(true);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		// MENU-------------------------
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("Nuevo juego");
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Selectornivel nivel = new Selectornivel();
				setVisible(false);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Salir");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenu mnNewMenu_1 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Instrucciones");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Bienvenido a mastermind, para empezar a jugar clica en un color de los colores disponibles \n"
						+ "después clica en un cuadrado de los guesses y cuando lo tengas completado, dale al botón comp para ver si es correcto o no.");
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Acerca de...");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Colores disponibles", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1.setBounds(537, 30, 296, 88);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		Panel panel = new Panel();
		panel.setBounds(6, 18, 284, 63);
		panel_1.add(panel);
		panel.setLayout(null);
		
		
		// BOTONES GUESS-------------------
		final JButton btnColor1 = new JButton("");
		btnColor1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor1.getBackground();
			}
		});
		btnColor1.setBackground(colorDisp1);
		btnColor1.setBounds(12, 25, 24, 25);
		panel.add(btnColor1);
		
		final JButton btnColor2 = new JButton("");
		btnColor2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor2.getBackground();
			}
		});
		btnColor2.setBackground(colorDisp2);
		btnColor2.setBounds(48, 25, 24, 25);
		panel.add(btnColor2);
		
		final JButton btnColor3 = new JButton("");
		btnColor3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor3.getBackground();
			}
		});
		btnColor3.setBackground(colorDisp3);
		btnColor3.setBounds(84, 25, 24, 25);
		panel.add(btnColor3);
		
		final JButton btnColor4 = new JButton("");
		btnColor4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor4.getBackground();
			}
		});
		btnColor4.setBackground(colorDisp4);
		btnColor4.setBounds(120, 25, 24, 25);
		panel.add(btnColor4);
		
		final JButton btnColor5 = new JButton("");
		btnColor5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor5.getBackground();
			}
		});
		btnColor5.setBackground(colorDisp5);
		btnColor5.setBounds(156, 25, 24, 25);
		panel.add(btnColor5);
		
		final JButton btnColor6 = new JButton("");
		btnColor6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor6.getBackground();
			}
		});
		btnColor6.setBackground(colorDisp6);
		btnColor6.setBounds(192, 25, 24, 25);
		panel.add(btnColor6);
		
		Color matrizColores[] = new Color[6];
		
		for (int i = 0; i < matrizColores.length; i++) {
			if(i == 0) {
				matrizColores[i] = btnColor1.getBackground();
			}
			else if (i == 1) {
				matrizColores[i] = btnColor2.getBackground();
			}
			else if (i == 2) {
				matrizColores[i] = btnColor3.getBackground();
			}
			else if (i == 3) {
				matrizColores[i] = btnColor4.getBackground();
			}
			else if (i == 4) {
				matrizColores[i] = btnColor5.getBackground();
			}
			else {
				matrizColores[i] = btnColor6.getBackground();
			}
		}
		
		
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setLayout(null);
		panel_1_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Combinaci\u00F3n secreta", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1_1.setBounds(537, 147, 296, 88);
		contentPane.add(panel_1_1);
		
		Panel panel_2 = new Panel();
		panel_2.setLayout(null);
		panel_2.setBounds(6, 18, 284, 63);
		panel_1_1.add(panel_2);
		
		// BOTONES SECRETOS--------------------
		final JButton btnSec1 = new JButton("");
		btnSec1.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec1.setBounds(12, 25, 24, 25);
		panel_2.add(btnSec1);
		
		final JButton btnSec2 = new JButton("");
		btnSec2.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec2.setBounds(48, 25, 24, 25);
		panel_2.add(btnSec2);
		
		final JButton btnSec3 = new JButton("");
		btnSec3.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec3.setBounds(84, 25, 24, 25);
		panel_2.add(btnSec3);
		
		final JButton btnSec4 = new JButton("");
		btnSec4.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec4.setBounds(120, 25, 24, 25);
		panel_2.add(btnSec4);
		
		arrayBotonesSecuencia[0] = btnSec1;
		arrayBotonesSecuencia[1] = btnSec2;
		arrayBotonesSecuencia[2] = btnSec3;
		arrayBotonesSecuencia[3] = btnSec4;
		
		colorSec1 = btnSec1.getBackground();
		colorSec2 = btnSec2.getBackground();
		colorSec3 = btnSec3.getBackground();
		colorSec4 = btnSec4.getBackground();
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Guesses", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(6, 12, 144, 240);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		
		// BOTONES GUESS-----------------------------------
		final JButton btn1Sec1 = new JButton("");
		btn1Sec1.setBounds(6, 18, 24, 25);
		panel_3.add(btn1Sec1);
		btn1Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec1.setBackground(colorBoton);
			}
		});
		btn1Sec1.setBackground(Color.LIGHT_GRAY);
		
		arrayBotonesResp1[0] = btn1Sec1;
		
		final JButton btn2Sec1 = new JButton("");
		btn2Sec1.setBounds(42, 18, 24, 25);
		panel_3.add(btn2Sec1);
		btn2Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec1.setBackground(colorBoton);
			}
		});
		btn2Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[1] = btn2Sec1;
		
		final JButton btn3Sec1 = new JButton("");
		btn3Sec1.setBounds(78, 18, 24, 25);
		panel_3.add(btn3Sec1);
		btn3Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec1.setBackground(colorBoton);
			}
		});
		btn3Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[2] = btn3Sec1;
		
		final JButton btn4Sec1 = new JButton("");
		btn4Sec1.setBounds(114, 18, 24, 25);
		panel_3.add(btn4Sec1);
		btn4Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec1.setBackground(colorBoton);
			}
		});
		btn4Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[3] = btn4Sec1;
		
		final JButton btn1Sec2 = new JButton("");
		btn1Sec2.setBounds(6, 56, 24, 25);
		panel_3.add(btn1Sec2);
		btn1Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec2.setBackground(colorBoton);
			}
		});
		btn1Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec2 = new JButton("");
		btn2Sec2.setBounds(42, 56, 24, 25);
		panel_3.add(btn2Sec2);
		btn2Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec2.setBackground(colorBoton);
			}
		});
		btn2Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec2 = new JButton("");
		btn3Sec2.setBounds(78, 56, 24, 25);
		panel_3.add(btn3Sec2);
		btn3Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec2.setBackground(colorBoton);
			}
		});
		btn3Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec2 = new JButton("");
		btn4Sec2.setBounds(114, 56, 24, 25);
		panel_3.add(btn4Sec2);
		btn4Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec2.setBackground(colorBoton);
			}
		});
		btn4Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec3 = new JButton("");
		btn1Sec3.setBounds(6, 94, 24, 25);
		panel_3.add(btn1Sec3);
		btn1Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec3.setBackground(colorBoton);
			}
		});
		btn1Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec3 = new JButton("");
		btn2Sec3.setBounds(42, 94, 24, 25);
		panel_3.add(btn2Sec3);
		btn2Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec3.setBackground(colorBoton);
			}
		});
		btn2Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec3 = new JButton("");
		btn3Sec3.setBounds(78, 94, 24, 25);
		panel_3.add(btn3Sec3);
		btn3Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec3.setBackground(colorBoton);
			}
		});
		btn3Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec3 = new JButton("");
		btn4Sec3.setBounds(114, 94, 24, 25);
		panel_3.add(btn4Sec3);
		btn4Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec3.setBackground(colorBoton);
			}
		});
		btn4Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec4 = new JButton("");
		btn1Sec4.setBounds(6, 132, 24, 25);
		panel_3.add(btn1Sec4);
		btn1Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec4.setBackground(colorBoton);
			}
		});
		btn1Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec4 = new JButton("");
		btn2Sec4.setBounds(42, 132, 24, 25);
		panel_3.add(btn2Sec4);
		btn2Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec4.setBackground(colorBoton);
			}
		});
		btn2Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec4 = new JButton("");
		btn3Sec4.setBounds(78, 132, 24, 25);
		panel_3.add(btn3Sec4);
		btn3Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec4.setBackground(colorBoton);
			}
		});
		btn3Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec4 = new JButton("");
		btn4Sec4.setBounds(114, 132, 24, 25);
		panel_3.add(btn4Sec4);
		btn4Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec4.setBackground(colorBoton);
			}
		});
		btn4Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec5 = new JButton("");
		btn1Sec5.setBounds(6, 170, 24, 25);
		panel_3.add(btn1Sec5);
		btn1Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec5.setBackground(colorBoton);
			}
		});
		btn1Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec5 = new JButton("");
		btn2Sec5.setBounds(42, 170, 24, 25);
		panel_3.add(btn2Sec5);
		btn2Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec5.setBackground(colorBoton);
			}
		});
		btn2Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec5 = new JButton("");
		btn3Sec5.setBounds(78, 170, 24, 25);
		panel_3.add(btn3Sec5);
		btn3Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec5.setBackground(colorBoton);
			}
		});
		btn3Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec5 = new JButton("");
		btn4Sec5.setBounds(114, 170, 24, 25);
		panel_3.add(btn4Sec5);
		btn4Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec5.setBackground(colorBoton);
			}
		});
		btn4Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec6 = new JButton("");
		btn1Sec6.setBounds(6, 208, 24, 25);
		panel_3.add(btn1Sec6);
		btn1Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec6.setBackground(colorBoton);
			}
		});
		btn1Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec6 = new JButton("");
		btn2Sec6.setBounds(42, 208, 24, 25);
		panel_3.add(btn2Sec6);
		btn2Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec6.setBackground(colorBoton);
			}
		});
		btn2Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec6 = new JButton("");
		btn3Sec6.setBounds(78, 208, 24, 25);
		panel_3.add(btn3Sec6);
		btn3Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec6.setBackground(colorBoton);
			}
		});
		btn3Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec6 = new JButton("");
		btn4Sec6.setBounds(114, 208, 24, 25);
		panel_3.add(btn4Sec6);
		btn4Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec6.setBackground(colorBoton);
			}
		});
		btn4Sec6.setBackground(Color.LIGHT_GRAY);
		btn4Sec6.setVisible(false);
		btn3Sec6.setVisible(false);
		btn2Sec6.setVisible(false);
		btn1Sec6.setVisible(false);
		btn4Sec5.setVisible(false);
		btn3Sec5.setVisible(false);
		btn2Sec5.setVisible(false);
		btn1Sec5.setVisible(false);
		btn4Sec4.setVisible(false);
		btn3Sec4.setVisible(false);
		btn2Sec4.setVisible(false);
		btn1Sec4.setVisible(false);
		btn4Sec3.setVisible(false);
		btn3Sec3.setVisible(false);
		btn2Sec3.setVisible(false);
		btn1Sec3.setVisible(false);
		btn4Sec2.setVisible(false);
		btn3Sec2.setVisible(false);
		btn2Sec2.setVisible(false);
		btn1Sec2.setVisible(false);
		
		// BOTONES COMP-----------------------
		JButton btnComp1 = new JButton("Comp");
		btnComp1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec1.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec1.getBackground().equals(btnSec2.getBackground()) || btn1Sec1.getBackground().equals(btnSec3.getBackground()) || btn1Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec1.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec1.getBackground().equals(btnSec1.getBackground()) || btn2Sec1.getBackground().equals(btnSec3.getBackground()) || btn2Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec1.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec1.getBackground().equals(btnSec2.getBackground()) || btn3Sec1.getBackground().equals(btnSec1.getBackground()) || btn3Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec1.getBackground().equals(btnSec2.getBackground()) || btn4Sec1.getBackground().equals(btnSec3.getBackground()) || btn4Sec1.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones[j+3].setBackground(Color.WHITE);
						}
					}
						
						
					
				}
				
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec2.setVisible(true);
					btn2Sec2.setVisible(true);
					btn3Sec2.setVisible(true);
					btn4Sec2.setVisible(true);
					btnComp2.setVisible(true);
					btnResp1Sec2.setVisible(true);
					btnResp2Sec2.setVisible(true);
					btnResp3Sec2.setVisible(true);
					btnResp4Sec2.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec2.setVisible(true);
					btn2Sec2.setVisible(true);
					btn3Sec2.setVisible(true);
					btn4Sec2.setVisible(true);
					btnComp2.setVisible(true);
					btnResp1Sec2.setVisible(true);
					btnResp2Sec2.setVisible(true);
					btnResp3Sec2.setVisible(true);
					btnResp4Sec2.setVisible(true);
				}
				
			}
		});
		btnComp1.setBounds(178, 30, 71, 25);
		contentPane.add(btnComp1);
		
		 btnComp2 = new JButton("Comp");
		btnComp2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int j = 0; j < arrayBotones2.length; j++) {
					
					if (j == 0) {
						if (btn1Sec2.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones2[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec2.getBackground().equals(btnSec2.getBackground()) || btn1Sec2.getBackground().equals(btnSec3.getBackground()) || btn1Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec2.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones2[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec2.getBackground().equals(btnSec1.getBackground()) || btn2Sec2.getBackground().equals(btnSec3.getBackground()) || btn2Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec2.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones2[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec2.getBackground().equals(btnSec2.getBackground()) || btn3Sec2.getBackground().equals(btnSec2.getBackground()) || btn3Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec2.getBackground().equals(btnSec2.getBackground()) || btn4Sec2.getBackground().equals(btnSec3.getBackground()) || btn4Sec2.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones2[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				if (arrayBotones2[0].getBackground().equals(Color.BLACK) && arrayBotones2[1].getBackground().equals(Color.BLACK) && arrayBotones2[2].getBackground().equals(Color.BLACK) && arrayBotones2[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec3.setVisible(true);
					btn2Sec3.setVisible(true);
					btn3Sec3.setVisible(true);
					btn4Sec3.setVisible(true);
					btnComp3.setVisible(true);
					btnResp1Sec3.setVisible(true);
					btnResp2Sec3.setVisible(true);
					btnResp3Sec3.setVisible(true);
					btnResp4Sec3.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec3.setVisible(true);
					btn2Sec3.setVisible(true);
					btn3Sec3.setVisible(true);
					btn4Sec3.setVisible(true);
					btnComp3.setVisible(true);
					btnResp1Sec3.setVisible(true);
					btnResp2Sec3.setVisible(true);
					btnResp3Sec3.setVisible(true);
					btnResp4Sec3.setVisible(true);
				}
			}
		});
		btnComp2.setBounds(178, 68, 71, 25);
		contentPane.add(btnComp2);
		btnComp2.setVisible(false);
		
		 btnComp3 = new JButton("Comp");
		btnComp3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int j = 0; j < arrayBotones3.length; j++) {
					
					if (j == 0) {
						if (btn1Sec3.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones3[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec3.getBackground().equals(btnSec2.getBackground()) || btn1Sec3.getBackground().equals(btnSec3.getBackground()) || btn1Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec3.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones3[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec3.getBackground().equals(btnSec1.getBackground()) || btn2Sec3.getBackground().equals(btnSec3.getBackground()) || btn2Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec3.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones3[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec3.getBackground().equals(btnSec2.getBackground()) || btn3Sec3.getBackground().equals(btnSec1.getBackground()) || btn3Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec3.getBackground().equals(btnSec2.getBackground()) || btn4Sec3.getBackground().equals(btnSec3.getBackground()) || btn4Sec3.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones3[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				
				if (arrayBotones3[0].getBackground().equals(Color.BLACK) && arrayBotones3[1].getBackground().equals(Color.BLACK) && arrayBotones3[2].getBackground().equals(Color.BLACK) && arrayBotones3[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec4.setVisible(true);
					btn2Sec4.setVisible(true);
					btn3Sec4.setVisible(true);
					btn4Sec4.setVisible(true);
					btnComp4.setVisible(true);
					btnResp1Sec4.setVisible(true);
					btnResp2Sec4.setVisible(true);
					btnResp3Sec4.setVisible(true);
					btnResp4Sec4.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec4.setVisible(true);
					btn2Sec4.setVisible(true);
					btn3Sec4.setVisible(true);
					btn4Sec4.setVisible(true);
					btnComp4.setVisible(true);
					btnResp1Sec4.setVisible(true);
					btnResp2Sec4.setVisible(true);
					btnResp3Sec4.setVisible(true);
					btnResp4Sec4.setVisible(true);
				}
			}
		});
		btnComp3.setBounds(178, 106, 71, 25);
		contentPane.add(btnComp3);
		btnComp3.setVisible(false);
		
		 btnComp4 = new JButton("Comp");
		btnComp4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones4.length; j++) {
					
					if (j == 0) {
						if (btn1Sec4.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones4[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec4.getBackground().equals(btnSec4.getBackground()) || btn1Sec4.getBackground().equals(btnSec3.getBackground()) || btn1Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec4.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones4[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec4.getBackground().equals(btnSec1.getBackground()) || btn2Sec4.getBackground().equals(btnSec3.getBackground()) || btn2Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec4.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones4[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec4.getBackground().equals(btnSec2.getBackground()) || btn3Sec4.getBackground().equals(btnSec1.getBackground()) || btn3Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec4.getBackground().equals(btnSec2.getBackground()) || btn4Sec4.getBackground().equals(btnSec3.getBackground()) || btn4Sec4.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones4[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				if (arrayBotones4[0].getBackground().equals(Color.BLACK) && arrayBotones4[1].getBackground().equals(Color.BLACK) && arrayBotones4[2].getBackground().equals(Color.BLACK) && arrayBotones4[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec5.setVisible(true);
					btn2Sec5.setVisible(true);
					btn3Sec5.setVisible(true);
					btn4Sec5.setVisible(true);
					btnComp5.setVisible(true);
					btnResp1Sec5.setVisible(true);
					btnResp2Sec5.setVisible(true);
					btnResp3Sec5.setVisible(true);
					btnResp4Sec5.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec5.setVisible(true);
					btn2Sec5.setVisible(true);
					btn3Sec5.setVisible(true);
					btn4Sec5.setVisible(true);
					btnComp5.setVisible(true);
					btnResp1Sec5.setVisible(true);
					btnResp2Sec5.setVisible(true);
					btnResp3Sec5.setVisible(true);
					btnResp4Sec5.setVisible(true);
				}
			}
		});
		btnComp4.setBounds(178, 143, 71, 25);
		contentPane.add(btnComp4);
		btnComp4.setVisible(false);
		
		 btnComp5 = new JButton("Comp");
		btnComp5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones5.length; j++) {
					
					if (j == 0) {
						
						if (btn1Sec5.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones5[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec5.getBackground().equals(btnSec2.getBackground()) || btn1Sec5.getBackground().equals(btnSec3.getBackground()) || btn1Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec5.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones5[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec5.getBackground().equals(btnSec1.getBackground()) || btn2Sec5.getBackground().equals(btnSec3.getBackground()) || btn2Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec5.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones5[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec5.getBackground().equals(btnSec2.getBackground()) || btn3Sec5.getBackground().equals(btnSec1.getBackground()) || btn3Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec5.getBackground().equals(btnSec2.getBackground()) || btn4Sec5.getBackground().equals(btnSec3.getBackground()) || btn4Sec5.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones5[j+3].setBackground(Color.WHITE);
						}
					}
						
				
			}
				if (arrayBotones5[0].getBackground().equals(Color.BLACK) && arrayBotones5[1].getBackground().equals(Color.BLACK) && arrayBotones5[2].getBackground().equals(Color.BLACK) && arrayBotones5[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec6.setVisible(true);
					btn2Sec6.setVisible(true);
					btn3Sec6.setVisible(true);
					btn4Sec6.setVisible(true);
					btnComp6.setVisible(true);
					btnResp1Sec6.setVisible(true);
					btnResp2Sec6.setVisible(true);
					btnResp3Sec6.setVisible(true);
					btnResp4Sec6.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec6.setVisible(true);
					btn2Sec6.setVisible(true);
					btn3Sec6.setVisible(true);
					btn4Sec6.setVisible(true);
					btnComp6.setVisible(true);
					btnResp1Sec6.setVisible(true);
					btnResp2Sec6.setVisible(true);
					btnResp3Sec6.setVisible(true);
					btnResp4Sec6.setVisible(true);
				}
			}
		});
		btnComp5.setBounds(178, 182, 71, 25);
		contentPane.add(btnComp5);
		btnComp5.setVisible(false);
		
		 btnComp6 = new JButton("Comp");
		btnComp6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones6.length; j++) {
					
					if (j == 0) {
						if (btn1Sec6.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones6[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec6.getBackground().equals(btnSec2.getBackground()) || btn1Sec6.getBackground().equals(btnSec3.getBackground()) || btn1Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec6.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones6[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec6.getBackground().equals(btnSec1.getBackground()) || btn2Sec6.getBackground().equals(btnSec3.getBackground()) || btn2Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec6.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones6[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec6.getBackground().equals(btnSec2.getBackground()) || btn3Sec6.getBackground().equals(btnSec1.getBackground()) || btn3Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec6.getBackground().equals(btnSec2.getBackground()) || btn4Sec6.getBackground().equals(btnSec3.getBackground()) || btn4Sec6.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones6[j+3].setBackground(Color.WHITE);
						}
						
					}
					
					
				
			}
				if (arrayBotones6[0].getBackground().equals(Color.BLACK) && arrayBotones6[1].getBackground().equals(Color.BLACK) && arrayBotones6[2].getBackground().equals(Color.BLACK) && arrayBotones6[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!, empieza otra partida");
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
					
				}
				else {
					JOptionPane.showMessageDialog(null, "Has perdido, empieza otra partida");
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				
				}
			}
		});
		btnComp6.setBounds(178, 220, 71, 25);
		contentPane.add(btnComp6);
		btnComp6.setVisible(false);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Soluciones", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(282, 12, 144, 240);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		// BOTONES RESPUESTA-------------------------------------
		btnResp1Sec1 = new JButton("");
		btnResp1Sec1.setBounds(6, 18, 24, 25);
		panel_4.add(btnResp1Sec1);
		btnResp1Sec1.setBackground(Color.LIGHT_GRAY);
		
		arrayBotones[0] = btnResp1Sec1;
		
		btnResp2Sec1 = new JButton("");
		btnResp2Sec1.setBounds(42, 18, 24, 25);
		panel_4.add(btnResp2Sec1);
		btnResp2Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[1] = btnResp2Sec1;
		
		btnResp3Sec1 = new JButton("");
		btnResp3Sec1.setBounds(78, 18, 24, 25);
		panel_4.add(btnResp3Sec1);
		btnResp3Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[2] = btnResp3Sec1;
		
		btnResp4Sec1 = new JButton("");
		btnResp4Sec1.setBounds(114, 18, 24, 25);
		panel_4.add(btnResp4Sec1);
		btnResp4Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[3] = btnResp4Sec1;
		
		btnResp1Sec2 = new JButton("");
		btnResp1Sec2.setBounds(6, 56, 24, 25);
		panel_4.add(btnResp1Sec2);
		btnResp1Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec2.setVisible(false);
		
		arrayBotones2[0] = btnResp1Sec2;
		
		btnResp2Sec2 = new JButton("");
		btnResp2Sec2.setBounds(42, 56, 24, 25);
		panel_4.add(btnResp2Sec2);
		btnResp2Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec2.setVisible(false);
		arrayBotones2[1] = btnResp2Sec2;
		
		btnResp3Sec2 = new JButton("");
		btnResp3Sec2.setBounds(78, 56, 24, 25);
		panel_4.add(btnResp3Sec2);
		btnResp3Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec2.setVisible(false);
		arrayBotones2[2] = btnResp3Sec2;
		
		 btnResp4Sec2 = new JButton("");
		 btnResp4Sec2.setBounds(114, 56, 24, 25);
		 panel_4.add(btnResp4Sec2);
		btnResp4Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec2.setVisible(false);
		arrayBotones2[3] = btnResp4Sec2;
		
		 btnResp1Sec3 = new JButton("");
		 btnResp1Sec3.setBounds(6, 94, 24, 25);
		 panel_4.add(btnResp1Sec3);
		btnResp1Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec3.setVisible(false);
		
		arrayBotones3[0] = btnResp1Sec3;
		
		 btnResp2Sec3 = new JButton("");
		 btnResp2Sec3.setBounds(42, 94, 24, 25);
		 panel_4.add(btnResp2Sec3);
		btnResp2Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec3.setVisible(false);
		arrayBotones3[1] = btnResp2Sec3;
		
		btnResp3Sec3 = new JButton("");
		btnResp3Sec3.setBounds(78, 94, 24, 25);
		panel_4.add(btnResp3Sec3);
		btnResp3Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec3.setVisible(false);
		arrayBotones3[2] = btnResp3Sec3;
		
		btnResp4Sec3 = new JButton("");
		btnResp4Sec3.setBounds(114, 94, 24, 25);
		panel_4.add(btnResp4Sec3);
		btnResp4Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec3.setVisible(false);
		arrayBotones3[3] = btnResp4Sec3;
		
		btnResp1Sec4 = new JButton("");
		btnResp1Sec4.setBounds(6, 132, 24, 25);
		panel_4.add(btnResp1Sec4);
		btnResp1Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec4.setVisible(false);
		
		arrayBotones4[0] = btnResp1Sec4;
		
		 btnResp2Sec4 = new JButton("");
		 btnResp2Sec4.setBounds(42, 132, 24, 25);
		 panel_4.add(btnResp2Sec4);
		btnResp2Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec4.setVisible(false);
		arrayBotones4[1] = btnResp2Sec4;
		
		 btnResp3Sec4 = new JButton("");
		 btnResp3Sec4.setBounds(78, 132, 24, 25);
		 panel_4.add(btnResp3Sec4);
		btnResp3Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec4.setVisible(false);
		arrayBotones4[2] = btnResp3Sec4;
		
		 btnResp4Sec4 = new JButton("");
		 btnResp4Sec4.setBounds(114, 132, 24, 25);
		 panel_4.add(btnResp4Sec4);
		btnResp4Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec4.setVisible(false);
		arrayBotones4[3] = btnResp4Sec4;
		
		 btnResp1Sec5 = new JButton("");
		 btnResp1Sec5.setBounds(6, 170, 24, 25);
		 panel_4.add(btnResp1Sec5);
		btnResp1Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec5.setVisible(false);
		
		arrayBotones5[0] = btnResp1Sec5;
		
		 btnResp2Sec5 = new JButton("");
		 btnResp2Sec5.setBounds(42, 170, 24, 25);
		 panel_4.add(btnResp2Sec5);
		btnResp2Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec5.setVisible(false);
		arrayBotones5[1] = btnResp2Sec5;
		
		 btnResp3Sec5 = new JButton("");
		 btnResp3Sec5.setBounds(78, 170, 24, 25);
		 panel_4.add(btnResp3Sec5);
		btnResp3Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec5.setVisible(false);
		arrayBotones5[2] = btnResp3Sec5;
		
		 btnResp4Sec5 = new JButton("");
		 btnResp4Sec5.setBounds(114, 170, 24, 25);
		 panel_4.add(btnResp4Sec5);
		btnResp4Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec5.setVisible(false);
		arrayBotones5[3] = btnResp4Sec5;
		
		 btnResp1Sec6 = new JButton("");
		 btnResp1Sec6.setBounds(6, 208, 24, 25);
		 panel_4.add(btnResp1Sec6);
		btnResp1Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec6.setVisible(false);
		
		arrayBotones6[0] = btnResp1Sec6;
		
		 btnResp2Sec6 = new JButton("");
		 btnResp2Sec6.setBounds(42, 208, 24, 25);
		 panel_4.add(btnResp2Sec6);
		btnResp2Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec6.setVisible(false);
		arrayBotones6[1] = btnResp2Sec6;
		
		 btnResp3Sec6 = new JButton("");
		 btnResp3Sec6.setBounds(78, 208, 24, 25);
		 panel_4.add(btnResp3Sec6);
		btnResp3Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec6.setVisible(false);
		arrayBotones6[2] = btnResp3Sec6;
		
		 btnResp4Sec6 = new JButton("");
		 btnResp4Sec6.setBounds(114, 208, 24, 25);
		 panel_4.add(btnResp4Sec6);
		btnResp4Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec6.setVisible(false);
		arrayBotones6[3] = btnResp4Sec6;
		
	}

}
