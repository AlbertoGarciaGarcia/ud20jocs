package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Panel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JMenuBar;
import java.awt.Label;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.ScrollPane;
import java.awt.Button;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.util.Random;
import javax.swing.JToggleButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Principal extends JFrame {

	// Creo todos los objetos necesarios para modificarlos después
	private JPanel contentPane;
	
	private Color colorBoton;

	Random rand = new Random();
	
	JButton btnResp1Sec1;
	JButton btnResp2Sec1;
	JButton btnResp3Sec1;
	JButton btnResp4Sec1;
	
	JButton btnResp1Sec2;
	JButton btnResp2Sec2;
	JButton btnResp3Sec2;
	JButton btnResp4Sec2;
	
	JButton btnResp1Sec3;
	JButton btnResp2Sec3;
	JButton btnResp3Sec3;
	JButton btnResp4Sec3;
	
	JButton btnResp1Sec4;
	JButton btnResp2Sec4;
	JButton btnResp3Sec4;
	JButton btnResp4Sec4;
	
	JButton btnResp1Sec5;
	JButton btnResp2Sec5;
	JButton btnResp3Sec5;
	JButton btnResp4Sec5;
	
	JButton btnResp1Sec6;
	JButton btnResp2Sec6;
	JButton btnResp3Sec6;
	JButton btnResp4Sec6;
	
	JButton btnResp1Sec7;
	JButton btnResp2Sec7;
	JButton btnResp3Sec7;
	JButton btnResp4Sec7;
	
	JButton btnResp1Sec8;
	JButton btnResp2Sec8;
	JButton btnResp3Sec8;
	JButton btnResp4Sec8;
	
	JButton btnResp1Sec9;
	JButton btnResp2Sec9;
	JButton btnResp3Sec9;
	JButton btnResp4Sec9;
	
	JButton btnResp1Sec10;
	JButton btnResp2Sec10;
	JButton btnResp3Sec10;
	JButton btnResp4Sec10;
	
	JButton btnComp2;
	JButton btnComp3;
	JButton btnComp4;
	JButton btnComp5;
	JButton btnComp6;
	JButton btnComp7;
	JButton btnComp8;
	JButton btnComp9;
	JButton btnComp10;
	
	JButton[] arrayBotones = new JButton[4];
	JButton[] arrayBotones2 = new JButton[4];
	JButton[] arrayBotones3 = new JButton[4];
	JButton[] arrayBotones4 = new JButton[4];
	JButton[] arrayBotones5 = new JButton[4];
	JButton[] arrayBotones6 = new JButton[4];
	JButton[] arrayBotones7 = new JButton[4];
	JButton[] arrayBotones8 = new JButton[4];
	JButton[] arrayBotones9 = new JButton[4];
	JButton[] arrayBotones10 = new JButton[4];
	
	JButton[] arrayBotonesSecuencia = new JButton[4];
	JButton[] arrayBotonesResp1 = new JButton[4];
	
	Color colorDisp1 = Color.BLUE;
	Color colorDisp2 = Color.GREEN;
	Color colorDisp3 = Color.MAGENTA;
	Color colorDisp4 = Color.ORANGE;
	Color colorDisp5 = Color.PINK;
	Color colorDisp6 = Color.RED;
	
	Color colorSec1;
	Color colorSec2;
	Color colorSec3;
	Color colorSec4;
	
	int contAzul;
	int contVerde;
	int contMagenta;
	int contNaranja;
	int contRosa;
	int contRojo;
	
	int contAzulGuess1;
	int contVerdeGuess1;
	int contMagentaGuess1;
	int contNaranjaGuess1;
	int contRosaGuess1;
	int contRojoGuess1;
	
	public Principal() {
		// Creo el panel
		setTitle("MasterMind");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 908, 538);
		setVisible(true);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnNewMenu = new JMenu("Archivo");
		menuBar.add(mnNewMenu);
		
		// Creo una barra de menú donde habrá un botón de nuevo juego
		JMenuItem mntmNewMenuItem = new JMenuItem("Nuevo juego");
		// Que hará está acción
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Selectornivel nivel = new Selectornivel();
				setVisible(false);
			}
		});
		mnNewMenu.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Salir");
		mnNewMenu.add(mntmNewMenuItem_1);
		
		JMenu mnNewMenu_1 = new JMenu("Ayuda");
		menuBar.add(mnNewMenu_1);
		
		// Creo otra que habrán las instrucciones
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Instrucciones");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Bienvenido a mastermind, para empezar a jugar clica en un color de los colores disponibles \n"
						+ "después clica en un cuadrado de los guesses y cuando lo tengas completado, dale al botón comp para ver si es correcto o no.");
			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);
		
		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Acerca de...");
		mnNewMenu_1.add(mntmNewMenuItem_3);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Colores disponibles", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1.setBounds(537, 30, 296, 88);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		Panel panel = new Panel();
		panel.setBounds(6, 18, 284, 63);
		panel_1.add(panel);
		panel.setLayout(null);
		
		// Creo los botones donde irán los colores disponibles
		// En cada uno de ellos cogeremos el color cuando le demos al click
		final JButton btnColor1 = new JButton("");
		btnColor1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor1.getBackground();
			}
		});
		btnColor1.setBackground(colorDisp1);
		btnColor1.setBounds(12, 25, 24, 25);
		panel.add(btnColor1);
		
		final JButton btnColor2 = new JButton("");
		btnColor2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor2.getBackground();
			}
		});
		btnColor2.setBackground(colorDisp2);
		btnColor2.setBounds(48, 25, 24, 25);
		panel.add(btnColor2);
		
		final JButton btnColor3 = new JButton("");
		btnColor3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor3.getBackground();
			}
		});
		btnColor3.setBackground(colorDisp3);
		btnColor3.setBounds(84, 25, 24, 25);
		panel.add(btnColor3);
		
		final JButton btnColor4 = new JButton("");
		btnColor4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				colorBoton = btnColor4.getBackground();
			}
		});
		btnColor4.setBackground(colorDisp4);
		btnColor4.setBounds(120, 25, 24, 25);
		panel.add(btnColor4);
		
		Color matrizColores[] = new Color[4];
		
		for (int i = 0; i < matrizColores.length; i++) {
			if(i == 0) {
				matrizColores[i] = btnColor1.getBackground();
			}
			else if (i == 1) {
				matrizColores[i] = btnColor2.getBackground();
			}
			else if (i == 2) {
				matrizColores[i] = btnColor3.getBackground();
			}
			else {
				matrizColores[i] = btnColor4.getBackground();
			}
			
		}
		
		
		
		JPanel panel_1_1 = new JPanel();
		panel_1_1.setLayout(null);
		panel_1_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Combinaci\u00F3n secreta", TitledBorder.LEADING, TitledBorder.TOP, null, Color.BLUE));
		panel_1_1.setBounds(537, 147, 296, 88);
		contentPane.add(panel_1_1);
		
		Panel panel_2 = new Panel();
		panel_2.setLayout(null);
		panel_2.setBounds(6, 18, 284, 63);
		panel_1_1.add(panel_2);
		
		// Creo los botones los cuales serán los que contengan la secuencia secreta
		// Se rellenarán aleatoriamente entre los colores disponibles
		final JButton btnSec1 = new JButton("");
		btnSec1.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec1.setBounds(12, 25, 24, 25);
		panel_2.add(btnSec1);
		
		final JButton btnSec2 = new JButton("");
		btnSec2.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec2.setBounds(48, 25, 24, 25);
		panel_2.add(btnSec2);
		
		final JButton btnSec3 = new JButton("");
		btnSec3.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec3.setBounds(84, 25, 24, 25);
		panel_2.add(btnSec3);
		
		final JButton btnSec4 = new JButton("");
		btnSec4.setBackground(matrizColores[rand.nextInt(6)]);
		btnSec4.setBounds(120, 25, 24, 25);
		panel_2.add(btnSec4);
		
		// Los añado a un array
		arrayBotonesSecuencia[0] = btnSec1;
		arrayBotonesSecuencia[1] = btnSec2;
		arrayBotonesSecuencia[2] = btnSec3;
		arrayBotonesSecuencia[3] = btnSec4;
		
		colorSec1 = btnSec1.getBackground();
		colorSec2 = btnSec2.getBackground();
		colorSec3 = btnSec3.getBackground();
		colorSec4 = btnSec4.getBackground();
		
		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Guesses", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_3.setBounds(6, 12, 144, 392);
		contentPane.add(panel_3);
		panel_3.setLayout(null);
		
		
		// Creo los botones que formarán parte de las guesses
		final JButton btn1Sec1 = new JButton("");
		btn1Sec1.setBounds(6, 18, 24, 25);
		panel_3.add(btn1Sec1);
		btn1Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec1.setBackground(colorBoton);
			}
		});
		btn1Sec1.setBackground(Color.LIGHT_GRAY);
		
		arrayBotonesResp1[0] = btn1Sec1;
		
		final JButton btn2Sec1 = new JButton("");
		btn2Sec1.setBounds(42, 18, 24, 25);
		panel_3.add(btn2Sec1);
		btn2Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec1.setBackground(colorBoton);
			}
		});
		btn2Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[1] = btn2Sec1;
		
		final JButton btn3Sec1 = new JButton("");
		btn3Sec1.setBounds(78, 18, 24, 25);
		panel_3.add(btn3Sec1);
		btn3Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec1.setBackground(colorBoton);
			}
		});
		btn3Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[2] = btn3Sec1;
		
		final JButton btn4Sec1 = new JButton("");
		btn4Sec1.setBounds(114, 18, 24, 25);
		panel_3.add(btn4Sec1);
		btn4Sec1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec1.setBackground(colorBoton);
			}
		});
		btn4Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotonesResp1[3] = btn4Sec1;
		
		final JButton btn1Sec2 = new JButton("");
		btn1Sec2.setBounds(6, 56, 24, 25);
		panel_3.add(btn1Sec2);
		btn1Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec2.setBackground(colorBoton);
			}
		});
		btn1Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec2 = new JButton("");
		btn2Sec2.setBounds(42, 56, 24, 25);
		panel_3.add(btn2Sec2);
		btn2Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec2.setBackground(colorBoton);
			}
		});
		btn2Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec2 = new JButton("");
		btn3Sec2.setBounds(78, 56, 24, 25);
		panel_3.add(btn3Sec2);
		btn3Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec2.setBackground(colorBoton);
			}
		});
		btn3Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec2 = new JButton("");
		btn4Sec2.setBounds(114, 56, 24, 25);
		panel_3.add(btn4Sec2);
		btn4Sec2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec2.setBackground(colorBoton);
			}
		});
		btn4Sec2.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec3 = new JButton("");
		btn1Sec3.setBounds(6, 94, 24, 25);
		panel_3.add(btn1Sec3);
		btn1Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec3.setBackground(colorBoton);
			}
		});
		btn1Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec3 = new JButton("");
		btn2Sec3.setBounds(42, 94, 24, 25);
		panel_3.add(btn2Sec3);
		btn2Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec3.setBackground(colorBoton);
			}
		});
		btn2Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec3 = new JButton("");
		btn3Sec3.setBounds(78, 94, 24, 25);
		panel_3.add(btn3Sec3);
		btn3Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec3.setBackground(colorBoton);
			}
		});
		btn3Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec3 = new JButton("");
		btn4Sec3.setBounds(114, 94, 24, 25);
		panel_3.add(btn4Sec3);
		btn4Sec3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec3.setBackground(colorBoton);
			}
		});
		btn4Sec3.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec4 = new JButton("");
		btn1Sec4.setBounds(6, 132, 24, 25);
		panel_3.add(btn1Sec4);
		btn1Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec4.setBackground(colorBoton);
			}
		});
		btn1Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec4 = new JButton("");
		btn2Sec4.setBounds(42, 132, 24, 25);
		panel_3.add(btn2Sec4);
		btn2Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec4.setBackground(colorBoton);
			}
		});
		btn2Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec4 = new JButton("");
		btn3Sec4.setBounds(78, 132, 24, 25);
		panel_3.add(btn3Sec4);
		btn3Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec4.setBackground(colorBoton);
			}
		});
		btn3Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec4 = new JButton("");
		btn4Sec4.setBounds(114, 132, 24, 25);
		panel_3.add(btn4Sec4);
		btn4Sec4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec4.setBackground(colorBoton);
			}
		});
		btn4Sec4.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec5 = new JButton("");
		btn1Sec5.setBounds(6, 170, 24, 25);
		panel_3.add(btn1Sec5);
		btn1Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec5.setBackground(colorBoton);
			}
		});
		btn1Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec5 = new JButton("");
		btn2Sec5.setBounds(42, 170, 24, 25);
		panel_3.add(btn2Sec5);
		btn2Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec5.setBackground(colorBoton);
			}
		});
		btn2Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec5 = new JButton("");
		btn3Sec5.setBounds(78, 170, 24, 25);
		panel_3.add(btn3Sec5);
		btn3Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec5.setBackground(colorBoton);
			}
		});
		btn3Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec5 = new JButton("");
		btn4Sec5.setBounds(114, 170, 24, 25);
		panel_3.add(btn4Sec5);
		btn4Sec5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec5.setBackground(colorBoton);
			}
		});
		btn4Sec5.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec6 = new JButton("");
		btn1Sec6.setBounds(6, 208, 24, 25);
		panel_3.add(btn1Sec6);
		btn1Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec6.setBackground(colorBoton);
			}
		});
		btn1Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec6 = new JButton("");
		btn2Sec6.setBounds(42, 208, 24, 25);
		panel_3.add(btn2Sec6);
		btn2Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec6.setBackground(colorBoton);
			}
		});
		btn2Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec6 = new JButton("");
		btn3Sec6.setBounds(78, 208, 24, 25);
		panel_3.add(btn3Sec6);
		btn3Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec6.setBackground(colorBoton);
			}
		});
		btn3Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec6 = new JButton("");
		btn4Sec6.setBounds(114, 208, 24, 25);
		panel_3.add(btn4Sec6);
		btn4Sec6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec6.setBackground(colorBoton);
			}
		});
		btn4Sec6.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec7 = new JButton("");
		btn1Sec7.setBounds(6, 246, 24, 25);
		panel_3.add(btn1Sec7);
		btn1Sec7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec7.setBackground(colorBoton);
			}
		});
		btn1Sec7.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec7 = new JButton("");
		btn2Sec7.setBounds(42, 246, 24, 25);
		panel_3.add(btn2Sec7);
		btn2Sec7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec7.setBackground(colorBoton);
			}
		});
		btn2Sec7.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec7 = new JButton("");
		btn3Sec7.setBounds(78, 246, 24, 25);
		panel_3.add(btn3Sec7);
		btn3Sec7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec7.setBackground(colorBoton);
			}
		});
		btn3Sec7.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec7 = new JButton("");
		btn4Sec7.setBounds(114, 246, 24, 25);
		panel_3.add(btn4Sec7);
		btn4Sec7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec7.setBackground(colorBoton);
			}
		});
		btn4Sec7.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec8 = new JButton("");
		btn1Sec8.setBounds(6, 284, 24, 25);
		panel_3.add(btn1Sec8);
		btn1Sec8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec8.setBackground(colorBoton);
			}
		});
		btn1Sec8.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec8 = new JButton("");
		btn2Sec8.setBounds(42, 284, 24, 25);
		panel_3.add(btn2Sec8);
		btn2Sec8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec8.setBackground(colorBoton);
			}
		});
		btn2Sec8.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec8 = new JButton("");
		btn3Sec8.setBounds(78, 284, 24, 25);
		panel_3.add(btn3Sec8);
		btn3Sec8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec8.setBackground(colorBoton);
			}
		});
		btn3Sec8.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec8 = new JButton("");
		btn4Sec8.setBounds(114, 284, 24, 25);
		panel_3.add(btn4Sec8);
		btn4Sec8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec8.setBackground(colorBoton);
			}
		});
		btn4Sec8.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec9 = new JButton("");
		btn1Sec9.setBounds(6, 322, 24, 25);
		panel_3.add(btn1Sec9);
		btn1Sec9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec9.setBackground(colorBoton);
			}
		});
		btn1Sec9.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec9 = new JButton("");
		btn2Sec9.setBounds(42, 322, 24, 25);
		panel_3.add(btn2Sec9);
		btn2Sec9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec9.setBackground(colorBoton);
			}
		});
		btn2Sec9.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec9 = new JButton("");
		btn3Sec9.setBounds(78, 322, 24, 25);
		panel_3.add(btn3Sec9);
		btn3Sec9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec9.setBackground(colorBoton);
			}
		});
		btn3Sec9.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec9 = new JButton("");
		btn4Sec9.setBounds(114, 322, 24, 25);
		panel_3.add(btn4Sec9);
		btn4Sec9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec9.setBackground(colorBoton);
			}
		});
		btn4Sec9.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn1Sec10 = new JButton("");
		btn1Sec10.setBounds(6, 360, 24, 25);
		panel_3.add(btn1Sec10);
		btn1Sec10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn1Sec10.setBackground(colorBoton);
			}
		});
		btn1Sec10.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn2Sec10 = new JButton("");
		btn2Sec10.setBounds(42, 360, 24, 25);
		panel_3.add(btn2Sec10);
		btn2Sec10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn2Sec10.setBackground(colorBoton);
			}
		});
		btn2Sec10.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn3Sec10 = new JButton("");
		btn3Sec10.setBounds(78, 360, 24, 25);
		panel_3.add(btn3Sec10);
		btn3Sec10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn3Sec10.setBackground(colorBoton);
			}
		});
		btn3Sec10.setBackground(Color.LIGHT_GRAY);
		
		final JButton btn4Sec10 = new JButton("");
		btn4Sec10.setBounds(114, 360, 24, 25);
		panel_3.add(btn4Sec10);
		btn4Sec10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btn4Sec10.setBackground(colorBoton);
			}
		});
		btn4Sec10.setBackground(Color.LIGHT_GRAY);
		
		// Seteo todos los botones a false para que no se vean durante la ejecución del juego
		btn4Sec10.setVisible(false);
		btn3Sec10.setVisible(false);
		btn2Sec10.setVisible(false);
		btn1Sec10.setVisible(false);
		btn4Sec9.setVisible(false);
		btn3Sec9.setVisible(false);
		btn2Sec9.setVisible(false);
		btn1Sec9.setVisible(false);
		btn4Sec8.setVisible(false);
		btn3Sec8.setVisible(false);
		btn2Sec8.setVisible(false);
		btn1Sec8.setVisible(false);
		btn4Sec7.setVisible(false);
		btn3Sec7.setVisible(false);
		btn2Sec7.setVisible(false);
		btn1Sec7.setVisible(false);
		btn4Sec6.setVisible(false);
		btn3Sec6.setVisible(false);
		btn2Sec6.setVisible(false);
		btn1Sec6.setVisible(false);
		btn4Sec5.setVisible(false);
		btn3Sec5.setVisible(false);
		btn2Sec5.setVisible(false);
		btn1Sec5.setVisible(false);
		btn4Sec4.setVisible(false);
		btn3Sec4.setVisible(false);
		btn2Sec4.setVisible(false);
		btn1Sec4.setVisible(false);
		btn4Sec3.setVisible(false);
		btn3Sec3.setVisible(false);
		btn2Sec3.setVisible(false);
		btn1Sec3.setVisible(false);
		btn4Sec2.setVisible(false);
		btn3Sec2.setVisible(false);
		btn2Sec2.setVisible(false);
		btn1Sec2.setVisible(false);
		
		for (int j = 0; j < arrayBotonesResp1.length; j++) {
			if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp1)) {
				contAzulGuess1++;
			}
			else if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp2)) {
				contVerdeGuess1++;
			}
			else if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp3)) {
				contMagentaGuess1++;
			}
			else if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp4)) {
				contNaranjaGuess1++;
			}
			else if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp5)) {
				contRosaGuess1++;
			}
			else if (arrayBotonesSecuencia[j].getBackground().equals(colorDisp6)) {
				contRojoGuess1++;
			}
		}
		
//		BOTONES DE COMPROBACIÓN
		// Creo los botones de comprobación para ver si la guess es correcta
		JButton btnComp1 = new JButton("Comp");
		btnComp1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
		
				// Recorre las arrays de las respuestas del guess para pintarlos de blanco o negro
				// Según la guess
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec1.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec1.getBackground().equals(btnSec2.getBackground()) || btn1Sec1.getBackground().equals(btnSec3.getBackground()) || btn1Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec1.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec1.getBackground().equals(btnSec1.getBackground()) || btn2Sec1.getBackground().equals(btnSec3.getBackground()) || btn2Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec1.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec1.getBackground().equals(btnSec2.getBackground()) || btn3Sec1.getBackground().equals(btnSec1.getBackground()) || btn3Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec1.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec1.getBackground().equals(btnSec2.getBackground()) || btn4Sec1.getBackground().equals(btnSec3.getBackground()) || btn4Sec1.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones[j+3].setBackground(Color.WHITE);
						}
					}
						
						
					
				}
				
				// Y seguidamente hago visible todos los botones siguientes
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec2.setVisible(true);
					btn2Sec2.setVisible(true);
					btn3Sec2.setVisible(true);
					btn4Sec2.setVisible(true);
					btnComp2.setVisible(true);
					btnResp1Sec2.setVisible(true);
					btnResp2Sec2.setVisible(true);
					btnResp3Sec2.setVisible(true);
					btnResp4Sec2.setVisible(true);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec2.setVisible(true);
					btn2Sec2.setVisible(true);
					btn3Sec2.setVisible(true);
					btn4Sec2.setVisible(true);
					btnComp2.setVisible(true);
					btnResp1Sec2.setVisible(true);
					btnResp2Sec2.setVisible(true);
					btnResp3Sec2.setVisible(true);
					btnResp4Sec2.setVisible(true);
				}
				
			}
		});
		btnComp1.setBounds(178, 30, 71, 25);
		contentPane.add(btnComp1);
		
		// Y así todo el rato
		 btnComp2 = new JButton("Comp");
		btnComp2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec2.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones2[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec2.getBackground().equals(btnSec2.getBackground()) || btn1Sec2.getBackground().equals(btnSec3.getBackground()) || btn1Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec2.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones2[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec2.getBackground().equals(btnSec1.getBackground()) || btn2Sec2.getBackground().equals(btnSec3.getBackground()) || btn2Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec2.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones2[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec2.getBackground().equals(btnSec2.getBackground()) || btn3Sec2.getBackground().equals(btnSec2.getBackground()) || btn3Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec2.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones2[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec2.getBackground().equals(btnSec2.getBackground()) || btn4Sec2.getBackground().equals(btnSec3.getBackground()) || btn4Sec2.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones2[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec3.setVisible(true);
					btn2Sec3.setVisible(true);
					btn3Sec3.setVisible(true);
					btn4Sec3.setVisible(true);
					btnComp3.setVisible(true);
					btnResp1Sec3.setVisible(true);
					btnResp2Sec3.setVisible(true);
					btnResp3Sec3.setVisible(true);
					btnResp4Sec3.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec3.setVisible(true);
					btn2Sec3.setVisible(true);
					btn3Sec3.setVisible(true);
					btn4Sec3.setVisible(true);
					btnComp3.setVisible(true);
					btnResp1Sec3.setVisible(true);
					btnResp2Sec3.setVisible(true);
					btnResp3Sec3.setVisible(true);
					btnResp4Sec3.setVisible(true);
				}
			}
		});
		btnComp2.setBounds(178, 68, 71, 25);
		contentPane.add(btnComp2);
		btnComp2.setVisible(false);
		
		 btnComp3 = new JButton("Comp");
		btnComp3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec3.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones3[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec3.getBackground().equals(btnSec2.getBackground()) || btn1Sec3.getBackground().equals(btnSec3.getBackground()) || btn1Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec3.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones3[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec3.getBackground().equals(btnSec1.getBackground()) || btn2Sec3.getBackground().equals(btnSec3.getBackground()) || btn2Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec3.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones3[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec3.getBackground().equals(btnSec2.getBackground()) || btn3Sec3.getBackground().equals(btnSec1.getBackground()) || btn3Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec3.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones3[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec3.getBackground().equals(btnSec2.getBackground()) || btn4Sec3.getBackground().equals(btnSec3.getBackground()) || btn4Sec3.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones3[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec4.setVisible(true);
					btn2Sec4.setVisible(true);
					btn3Sec4.setVisible(true);
					btn4Sec4.setVisible(true);
					btnComp4.setVisible(true);
					btnResp1Sec4.setVisible(true);
					btnResp2Sec4.setVisible(true);
					btnResp3Sec4.setVisible(true);
					btnResp4Sec4.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec4.setVisible(true);
					btn2Sec4.setVisible(true);
					btn3Sec4.setVisible(true);
					btn4Sec4.setVisible(true);
					btnComp4.setVisible(true);
					btnResp1Sec4.setVisible(true);
					btnResp2Sec4.setVisible(true);
					btnResp3Sec4.setVisible(true);
					btnResp4Sec4.setVisible(true);
				}
			}
		});
		btnComp3.setBounds(178, 106, 71, 25);
		contentPane.add(btnComp3);
		btnComp3.setVisible(false);
		
		 btnComp4 = new JButton("Comp");
		btnComp4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec4.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones4[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec4.getBackground().equals(btnSec4.getBackground()) || btn1Sec4.getBackground().equals(btnSec3.getBackground()) || btn1Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec4.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones4[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec4.getBackground().equals(btnSec1.getBackground()) || btn2Sec4.getBackground().equals(btnSec3.getBackground()) || btn2Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec4.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones4[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec4.getBackground().equals(btnSec2.getBackground()) || btn3Sec4.getBackground().equals(btnSec1.getBackground()) || btn3Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec4.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones4[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec4.getBackground().equals(btnSec2.getBackground()) || btn4Sec4.getBackground().equals(btnSec3.getBackground()) || btn4Sec4.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones4[j+3].setBackground(Color.WHITE);
						}
					}
					
					
				
			}
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec5.setVisible(true);
					btn2Sec5.setVisible(true);
					btn3Sec5.setVisible(true);
					btn4Sec5.setVisible(true);
					btnComp5.setVisible(true);
					btnResp1Sec5.setVisible(true);
					btnResp2Sec5.setVisible(true);
					btnResp3Sec5.setVisible(true);
					btnResp4Sec5.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec5.setVisible(true);
					btn2Sec5.setVisible(true);
					btn3Sec5.setVisible(true);
					btn4Sec5.setVisible(true);
					btnComp5.setVisible(true);
					btnResp1Sec5.setVisible(true);
					btnResp2Sec5.setVisible(true);
					btnResp3Sec5.setVisible(true);
					btnResp4Sec5.setVisible(true);
				}
			}
		});
		btnComp4.setBounds(178, 143, 71, 25);
		contentPane.add(btnComp4);
		btnComp4.setVisible(false);
		
		 btnComp5 = new JButton("Comp");
		btnComp5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						
						if (btn1Sec5.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones5[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec5.getBackground().equals(btnSec2.getBackground()) || btn1Sec5.getBackground().equals(btnSec3.getBackground()) || btn1Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec5.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones5[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec5.getBackground().equals(btnSec1.getBackground()) || btn2Sec5.getBackground().equals(btnSec3.getBackground()) || btn2Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec5.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones5[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec5.getBackground().equals(btnSec2.getBackground()) || btn3Sec5.getBackground().equals(btnSec1.getBackground()) || btn3Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec5.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones5[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec5.getBackground().equals(btnSec2.getBackground()) || btn4Sec5.getBackground().equals(btnSec3.getBackground()) || btn4Sec5.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones5[j+3].setBackground(Color.WHITE);
						}
					}
						
				
			}
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec6.setVisible(true);
					btn2Sec6.setVisible(true);
					btn3Sec6.setVisible(true);
					btn4Sec6.setVisible(true);
					btnComp6.setVisible(true);
					btnResp1Sec6.setVisible(true);
					btnResp2Sec6.setVisible(true);
					btnResp3Sec6.setVisible(true);
					btnResp4Sec6.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec6.setVisible(true);
					btn2Sec6.setVisible(true);
					btn3Sec6.setVisible(true);
					btn4Sec6.setVisible(true);
					btnComp6.setVisible(true);
					btnResp1Sec6.setVisible(true);
					btnResp2Sec6.setVisible(true);
					btnResp3Sec6.setVisible(true);
					btnResp4Sec6.setVisible(true);
				}
			}
		});
		btnComp5.setBounds(178, 182, 71, 25);
		contentPane.add(btnComp5);
		btnComp5.setVisible(false);
		
		 btnComp6 = new JButton("Comp");
		btnComp6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec6.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones6[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec6.getBackground().equals(btnSec2.getBackground()) || btn1Sec6.getBackground().equals(btnSec3.getBackground()) || btn1Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec6.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones6[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec6.getBackground().equals(btnSec1.getBackground()) || btn2Sec6.getBackground().equals(btnSec3.getBackground()) || btn2Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec6.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones6[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec6.getBackground().equals(btnSec2.getBackground()) || btn3Sec6.getBackground().equals(btnSec1.getBackground()) || btn3Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec6.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones6[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec6.getBackground().equals(btnSec2.getBackground()) || btn4Sec6.getBackground().equals(btnSec3.getBackground()) || btn4Sec6.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones6[j+3].setBackground(Color.WHITE);
						}
						
					}
					
					
				
			}
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec7.setVisible(true);
					btn2Sec7.setVisible(true);
					btn3Sec7.setVisible(true);
					btn4Sec7.setVisible(true);
					btnComp7.setVisible(true);
					btnResp1Sec7.setVisible(true);
					btnResp2Sec7.setVisible(true);
					btnResp3Sec7.setVisible(true);
					btnResp4Sec7.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec7.setVisible(true);
					btn2Sec7.setVisible(true);
					btn3Sec7.setVisible(true);
					btn4Sec7.setVisible(true);
					btnComp7.setVisible(true);
					btnResp1Sec7.setVisible(true);
					btnResp2Sec7.setVisible(true);
					btnResp3Sec7.setVisible(true);
					btnResp4Sec7.setVisible(true);
				}
			}
		});
		btnComp6.setBounds(178, 220, 71, 25);
		contentPane.add(btnComp6);
		btnComp6.setVisible(false);
		
		 btnComp7 = new JButton("Comp");
		btnComp7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec7.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones7[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec7.getBackground().equals(btnSec2.getBackground()) || btn1Sec7.getBackground().equals(btnSec3.getBackground()) || btn1Sec7.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones7[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec7.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones7[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec7.getBackground().equals(btnSec1.getBackground()) || btn2Sec7.getBackground().equals(btnSec3.getBackground()) || btn2Sec7.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones7[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec7.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones7[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec7.getBackground().equals(btnSec2.getBackground()) || btn3Sec7.getBackground().equals(btnSec1.getBackground()) || btn3Sec7.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones7[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec7.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones7[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec7.getBackground().equals(btnSec2.getBackground()) || btn4Sec7.getBackground().equals(btnSec3.getBackground()) || btn4Sec7.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones7[j+3].setBackground(Color.WHITE);
						}
					}
					
				
			}
				if (arrayBotones[0].getBackground().equals(Color.BLACK) && arrayBotones[1].getBackground().equals(Color.BLACK) && arrayBotones[2].getBackground().equals(Color.BLACK) && arrayBotones[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec8.setVisible(true);
					btn2Sec8.setVisible(true);
					btn3Sec8.setVisible(true);
					btn4Sec8.setVisible(true);
					btnComp8.setVisible(true);
					btnResp1Sec8.setVisible(true);
					btnResp2Sec8.setVisible(true);
					btnResp3Sec8.setVisible(true);
					btnResp4Sec8.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec8.setVisible(true);
					btn2Sec8.setVisible(true);
					btn3Sec8.setVisible(true);
					btn4Sec8.setVisible(true);
					btnComp8.setVisible(true);
					btnResp1Sec8.setVisible(true);
					btnResp2Sec8.setVisible(true);
					btnResp3Sec8.setVisible(true);
					btnResp4Sec8.setVisible(true);
				}
			}
		});
		btnComp7.setBounds(178, 258, 71, 25);
		contentPane.add(btnComp7);
		btnComp7.setVisible(false);
		
		 btnComp8 = new JButton("Comp");
		btnComp8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones.length; j++) {
					
					if (j == 0) {
						if (btn1Sec8.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones8[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec8.getBackground().equals(btnSec2.getBackground()) || btn1Sec8.getBackground().equals(btnSec3.getBackground()) || btn1Sec8.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones8[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec8.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones8[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec8.getBackground().equals(btnSec1.getBackground()) || btn2Sec8.getBackground().equals(btnSec3.getBackground()) || btn2Sec8.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones8[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec8.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones8[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec8.getBackground().equals(btnSec2.getBackground()) || btn3Sec8.getBackground().equals(btnSec1.getBackground()) || btn3Sec8.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones8[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec8.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones8[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec8.getBackground().equals(btnSec2.getBackground()) || btn4Sec8.getBackground().equals(btnSec3.getBackground()) || btn4Sec8.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones8[j+3].setBackground(Color.WHITE);
						}
					}
					
					
					
				
			}
				
				if (arrayBotones8[0].getBackground().equals(Color.BLACK) && arrayBotones8[1].getBackground().equals(Color.BLACK) && arrayBotones8[2].getBackground().equals(Color.BLACK) && arrayBotones8[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec9.setVisible(true);
					btn2Sec9.setVisible(true);
					btn3Sec9.setVisible(true);
					btn4Sec9.setVisible(true);
					btnComp9.setVisible(true);
					btnResp1Sec9.setVisible(true);
					btnResp2Sec9.setVisible(true);
					btnResp3Sec9.setVisible(true);
					btnResp4Sec9.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec9.setVisible(true);
					btn2Sec9.setVisible(true);
					btn3Sec9.setVisible(true);
					btn4Sec9.setVisible(true);
					btnComp9.setVisible(true);
					btnResp1Sec9.setVisible(true);
					btnResp2Sec9.setVisible(true);
					btnResp3Sec9.setVisible(true);
					btnResp4Sec9.setVisible(true);
				}
			}
		});
		btnComp8.setBounds(178, 296, 71, 25);
		contentPane.add(btnComp8);
		btnComp8.setVisible(false);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Soluciones", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_4.setBounds(282, 12, 144, 392);
		contentPane.add(panel_4);
		panel_4.setLayout(null);
		
		btnResp1Sec1 = new JButton("");
		btnResp1Sec1.setBounds(6, 18, 24, 25);
		panel_4.add(btnResp1Sec1);
		btnResp1Sec1.setBackground(Color.LIGHT_GRAY);
		
		arrayBotones[0] = btnResp1Sec1;
		
		btnResp2Sec1 = new JButton("");
		btnResp2Sec1.setBounds(42, 18, 24, 25);
		panel_4.add(btnResp2Sec1);
		btnResp2Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[1] = btnResp2Sec1;
		
		btnResp3Sec1 = new JButton("");
		btnResp3Sec1.setBounds(78, 18, 24, 25);
		panel_4.add(btnResp3Sec1);
		btnResp3Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[2] = btnResp3Sec1;
		
		btnResp4Sec1 = new JButton("");
		btnResp4Sec1.setBounds(114, 18, 24, 25);
		panel_4.add(btnResp4Sec1);
		btnResp4Sec1.setBackground(Color.LIGHT_GRAY);
		arrayBotones[3] = btnResp4Sec1;
		
		btnResp1Sec2 = new JButton("");
		btnResp1Sec2.setBounds(6, 56, 24, 25);
		panel_4.add(btnResp1Sec2);
		btnResp1Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec2.setVisible(false);
		
		arrayBotones2[0] = btnResp1Sec2;
		
		btnResp2Sec2 = new JButton("");
		btnResp2Sec2.setBounds(42, 56, 24, 25);
		panel_4.add(btnResp2Sec2);
		btnResp2Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec2.setVisible(false);
		arrayBotones2[1] = btnResp2Sec2;
		
		btnResp3Sec2 = new JButton("");
		btnResp3Sec2.setBounds(78, 56, 24, 25);
		panel_4.add(btnResp3Sec2);
		btnResp3Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec2.setVisible(false);
		arrayBotones2[2] = btnResp3Sec2;
		
		 btnResp4Sec2 = new JButton("");
		 btnResp4Sec2.setBounds(114, 56, 24, 25);
		 panel_4.add(btnResp4Sec2);
		btnResp4Sec2.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec2.setVisible(false);
		arrayBotones2[3] = btnResp4Sec2;
		
		 btnResp1Sec3 = new JButton("");
		 btnResp1Sec3.setBounds(6, 94, 24, 25);
		 panel_4.add(btnResp1Sec3);
		btnResp1Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec3.setVisible(false);
		
		arrayBotones3[0] = btnResp1Sec3;
		
		 btnResp2Sec3 = new JButton("");
		 btnResp2Sec3.setBounds(42, 94, 24, 25);
		 panel_4.add(btnResp2Sec3);
		btnResp2Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec3.setVisible(false);
		arrayBotones3[1] = btnResp2Sec3;
		
		btnResp3Sec3 = new JButton("");
		btnResp3Sec3.setBounds(78, 94, 24, 25);
		panel_4.add(btnResp3Sec3);
		btnResp3Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec3.setVisible(false);
		arrayBotones3[2] = btnResp3Sec3;
		
		btnResp4Sec3 = new JButton("");
		btnResp4Sec3.setBounds(114, 94, 24, 25);
		panel_4.add(btnResp4Sec3);
		btnResp4Sec3.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec3.setVisible(false);
		arrayBotones3[3] = btnResp4Sec3;
		
		btnResp1Sec4 = new JButton("");
		btnResp1Sec4.setBounds(6, 132, 24, 25);
		panel_4.add(btnResp1Sec4);
		btnResp1Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec4.setVisible(false);
		
		arrayBotones4[0] = btnResp1Sec4;
		
		 btnResp2Sec4 = new JButton("");
		 btnResp2Sec4.setBounds(42, 132, 24, 25);
		 panel_4.add(btnResp2Sec4);
		btnResp2Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec4.setVisible(false);
		arrayBotones4[1] = btnResp2Sec4;
		
		 btnResp3Sec4 = new JButton("");
		 btnResp3Sec4.setBounds(78, 132, 24, 25);
		 panel_4.add(btnResp3Sec4);
		btnResp3Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec4.setVisible(false);
		arrayBotones4[2] = btnResp3Sec4;
		
		 btnResp4Sec4 = new JButton("");
		 btnResp4Sec4.setBounds(114, 132, 24, 25);
		 panel_4.add(btnResp4Sec4);
		btnResp4Sec4.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec4.setVisible(false);
		arrayBotones4[3] = btnResp4Sec4;
		
		 btnResp1Sec5 = new JButton("");
		 btnResp1Sec5.setBounds(6, 170, 24, 25);
		 panel_4.add(btnResp1Sec5);
		btnResp1Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec5.setVisible(false);
		
		arrayBotones5[0] = btnResp1Sec5;
		
		 btnResp2Sec5 = new JButton("");
		 btnResp2Sec5.setBounds(42, 170, 24, 25);
		 panel_4.add(btnResp2Sec5);
		btnResp2Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec5.setVisible(false);
		arrayBotones5[1] = btnResp2Sec5;
		
		 btnResp3Sec5 = new JButton("");
		 btnResp3Sec5.setBounds(78, 170, 24, 25);
		 panel_4.add(btnResp3Sec5);
		btnResp3Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec5.setVisible(false);
		arrayBotones5[2] = btnResp3Sec5;
		
		 btnResp4Sec5 = new JButton("");
		 btnResp4Sec5.setBounds(114, 170, 24, 25);
		 panel_4.add(btnResp4Sec5);
		btnResp4Sec5.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec5.setVisible(false);
		arrayBotones5[3] = btnResp4Sec5;
		
		 btnResp1Sec6 = new JButton("");
		 btnResp1Sec6.setBounds(6, 208, 24, 25);
		 panel_4.add(btnResp1Sec6);
		btnResp1Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec6.setVisible(false);
		
		arrayBotones6[0] = btnResp1Sec6;
		
		 btnResp2Sec6 = new JButton("");
		 btnResp2Sec6.setBounds(42, 208, 24, 25);
		 panel_4.add(btnResp2Sec6);
		btnResp2Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec6.setVisible(false);
		arrayBotones6[1] = btnResp2Sec6;
		
		 btnResp3Sec6 = new JButton("");
		 btnResp3Sec6.setBounds(78, 208, 24, 25);
		 panel_4.add(btnResp3Sec6);
		btnResp3Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec6.setVisible(false);
		arrayBotones6[2] = btnResp3Sec6;
		
		 btnResp4Sec6 = new JButton("");
		 btnResp4Sec6.setBounds(114, 208, 24, 25);
		 panel_4.add(btnResp4Sec6);
		btnResp4Sec6.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec6.setVisible(false);
		arrayBotones6[3] = btnResp4Sec6;
		
		 btnResp1Sec7 = new JButton("");
		 btnResp1Sec7.setBounds(6, 246, 24, 25);
		 panel_4.add(btnResp1Sec7);
		btnResp1Sec7.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec7.setVisible(false);
		
		arrayBotones7[0] = btnResp1Sec7;
		
		 btnResp2Sec7 = new JButton("");
		 btnResp2Sec7.setBounds(42, 246, 24, 25);
		 panel_4.add(btnResp2Sec7);
		btnResp2Sec7.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec7.setVisible(false);
		arrayBotones7[1] = btnResp2Sec7;
		
		 btnResp3Sec7 = new JButton("");
		 btnResp3Sec7.setBounds(78, 246, 24, 25);
		 panel_4.add(btnResp3Sec7);
		btnResp3Sec7.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec7.setVisible(false);
		arrayBotones7[2] = btnResp3Sec7;
		
		 btnResp4Sec7 = new JButton("");
		 btnResp4Sec7.setBounds(114, 246, 24, 25);
		 panel_4.add(btnResp4Sec7);
		btnResp4Sec7.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec7.setVisible(false);
		arrayBotones7[3] = btnResp4Sec7;
		
		 btnResp1Sec8 = new JButton("");
		 btnResp1Sec8.setBounds(6, 284, 24, 25);
		 panel_4.add(btnResp1Sec8);
		btnResp1Sec8.setBackground(Color.LIGHT_GRAY);
		btnResp1Sec8.setVisible(false);
		
		arrayBotones8[0] = btnResp1Sec8;
		
		 btnResp2Sec8 = new JButton("");
		 btnResp2Sec8.setBounds(42, 284, 24, 25);
		 panel_4.add(btnResp2Sec8);
		btnResp2Sec8.setBackground(Color.LIGHT_GRAY);
		btnResp2Sec8.setVisible(false);
		arrayBotones8[1] = btnResp2Sec8;
		
		 btnResp3Sec8 = new JButton("");
		 btnResp3Sec8.setBounds(78, 284, 24, 25);
		 panel_4.add(btnResp3Sec8);
		btnResp3Sec8.setBackground(Color.LIGHT_GRAY);
		btnResp3Sec8.setVisible(false);
		arrayBotones8[2] = btnResp3Sec8;
		
		 btnResp4Sec8 = new JButton("");
		 btnResp4Sec8.setBounds(114, 284, 24, 25);
		 panel_4.add(btnResp4Sec8);
		btnResp4Sec8.setBackground(Color.LIGHT_GRAY);
		btnResp4Sec8.setVisible(false);
		arrayBotones8[3] = btnResp4Sec8;
		
		 btnResp1Sec9 = new JButton("");
		 btnResp1Sec9.setBounds(6, 322, 24, 25);
		 panel_4.add(btnResp1Sec9);
		 btnResp1Sec9.setBackground(Color.LIGHT_GRAY);
		 btnResp1Sec9.setVisible(false);
		 
		 arrayBotones9[0] = btnResp1Sec9;
		 
		 arrayBotones10[0] = btnResp1Sec9;
		 
		  btnResp2Sec9 = new JButton("");
		  btnResp2Sec9.setBounds(42, 322, 24, 25);
		  panel_4.add(btnResp2Sec9);
		  btnResp2Sec9.setBackground(Color.LIGHT_GRAY);
		  btnResp2Sec9.setVisible(false);
		  arrayBotones9[1] = btnResp2Sec9;
		  arrayBotones10[1] = btnResp2Sec9;
		  
		   btnResp3Sec9 = new JButton("");
		   btnResp3Sec9.setBounds(78, 322, 24, 25);
		   panel_4.add(btnResp3Sec9);
		   btnResp3Sec9.setBackground(Color.LIGHT_GRAY);
		   btnResp3Sec9.setVisible(false);
		   arrayBotones9[2] = btnResp3Sec9;
		   arrayBotones10[2] = btnResp3Sec9;
		   
		    btnResp4Sec9 = new JButton("");
		    btnResp4Sec9.setBounds(114, 322, 24, 25);
		    panel_4.add(btnResp4Sec9);
		    btnResp4Sec9.addActionListener(new ActionListener() {
		    	public void actionPerformed(ActionEvent e) {
		    	}
		    });
		    btnResp4Sec9.setBackground(Color.LIGHT_GRAY);
		    btnResp4Sec9.setVisible(false);
		    arrayBotones9[3] = btnResp4Sec9;
		    arrayBotones10[3] = btnResp4Sec9;
		    
		     btnResp1Sec10 = new JButton("");
		     btnResp1Sec10.setBounds(6, 360, 24, 25);
		     panel_4.add(btnResp1Sec10);
		     btnResp1Sec10.setBackground(Color.LIGHT_GRAY);
		     
		      btnResp2Sec10 = new JButton("");
		      btnResp2Sec10.setBounds(42, 360, 24, 25);
		      panel_4.add(btnResp2Sec10);
		      btnResp2Sec10.setBackground(Color.LIGHT_GRAY);
		      
		       btnResp3Sec10 = new JButton("");
		       btnResp3Sec10.setBounds(78, 360, 24, 25);
		       panel_4.add(btnResp3Sec10);
		       btnResp3Sec10.setBackground(Color.LIGHT_GRAY);
		       
		        btnResp4Sec10 = new JButton("");
		        btnResp4Sec10.setBounds(114, 360, 24, 25);
		        panel_4.add(btnResp4Sec10);
		        btnResp4Sec10.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        	}
		        });
		        btnResp4Sec10.setBackground(Color.LIGHT_GRAY);
		        btnResp4Sec10.setVisible(false);
		       btnResp3Sec10.setVisible(false);
		      btnResp2Sec10.setVisible(false);
		     btnResp1Sec10.setVisible(false);
		
		 btnComp9 = new JButton("Comp");
		btnComp9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones9.length; j++) {
					
					if (j == 0) {
						if (btn1Sec9.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones9[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec9.getBackground().equals(btnSec2.getBackground()) || btn1Sec9.getBackground().equals(btnSec3.getBackground()) || btn1Sec9.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones9[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec9.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones9[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec9.getBackground().equals(btnSec1.getBackground()) || btn2Sec9.getBackground().equals(btnSec3.getBackground()) || btn2Sec9.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones9[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec9.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones9[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec9.getBackground().equals(btnSec2.getBackground()) || btn3Sec9.getBackground().equals(btnSec1.getBackground()) || btn3Sec9.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones9[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec9.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones9[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec9.getBackground().equals(btnSec2.getBackground()) || btn4Sec9.getBackground().equals(btnSec3.getBackground()) || btn4Sec9.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones9[j+3].setBackground(Color.WHITE);
						}
					}
					
				
			}
				if (arrayBotones9[0].getBackground().equals(Color.BLACK) && arrayBotones9[1].getBackground().equals(Color.BLACK) && arrayBotones9[2].getBackground().equals(Color.BLACK) && arrayBotones9[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!");
					btn1Sec10.setVisible(true);
					btn2Sec10.setVisible(true);
					btn3Sec10.setVisible(true);
					btn4Sec10.setVisible(true);
					btnComp10.setVisible(true);
					btnResp1Sec10.setVisible(true);
					btnResp2Sec10.setVisible(true);
					btnResp3Sec10.setVisible(true);
					btnResp4Sec10.setVisible(true);
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				}
				else {
					JOptionPane.showMessageDialog(null, "No lo has adivinado prueba otra vez...");
					btn1Sec10.setVisible(true);
					btn2Sec10.setVisible(true);
					btn3Sec10.setVisible(true);
					btn4Sec10.setVisible(true);
					btnComp10.setVisible(true);
					btnResp1Sec10.setVisible(true);
					btnResp2Sec10.setVisible(true);
					btnResp3Sec10.setVisible(true);
					btnResp4Sec10.setVisible(true);
				}
			}
		});
		btnComp9.setBounds(178, 334, 71, 25);
		contentPane.add(btnComp9);
		btnComp9.setVisible(false);
		
		
		
		 btnComp10 = new JButton("Comp");
		btnComp10.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int j = 0; j < arrayBotones9.length; j++) {
					
					if (j == 0) {
						if (btn1Sec10.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones10[j].setBackground(Color.BLACK);
						}
						else if (btn1Sec10.getBackground().equals(btnSec2.getBackground()) || btn1Sec10.getBackground().equals(btnSec3.getBackground()) || btn1Sec10.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones10[j].setBackground(Color.WHITE);
						}
						
						
						if (btn2Sec10.getBackground().equals(btnSec2.getBackground())) {
							arrayBotones10[j+1].setBackground(Color.BLACK);
						}
						else if (btn2Sec10.getBackground().equals(btnSec1.getBackground()) || btn2Sec10.getBackground().equals(btnSec3.getBackground()) || btn2Sec10.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones10[j+1].setBackground(Color.WHITE);
						}
						
						
						if (btn3Sec10.getBackground().equals(btnSec3.getBackground())) {
							arrayBotones10[j+2].setBackground(Color.BLACK);
						}
						else if (btn3Sec10.getBackground().equals(btnSec2.getBackground()) || btn3Sec10.getBackground().equals(btnSec1.getBackground()) || btn3Sec10.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones10[j+2].setBackground(Color.WHITE);
						}
						
						
						if (btn4Sec10.getBackground().equals(btnSec4.getBackground())) {
							arrayBotones10[j+3].setBackground(Color.BLACK);
						}
						else if (btn4Sec10.getBackground().equals(btnSec2.getBackground()) || btn4Sec10.getBackground().equals(btnSec3.getBackground()) || btn4Sec10.getBackground().equals(btnSec1.getBackground())) {
							arrayBotones10[j+3].setBackground(Color.WHITE);
						}
					}
					
				
			}
				if (arrayBotones10[0].getBackground().equals(Color.BLACK) && arrayBotones10[1].getBackground().equals(Color.BLACK) && arrayBotones10[2].getBackground().equals(Color.BLACK) && arrayBotones10[3].getBackground().equals(Color.BLACK)) {
					JOptionPane.showMessageDialog(null, "Enhorabuena, has ganado!, empieza otra partida");
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
					
				}
				else {
					JOptionPane.showMessageDialog(null, "Has perdido, empieza otra partida");
					Selectornivel nivel = new Selectornivel();
					setVisible(false);
				
				}
			}
		});
		btnComp10.setBounds(178, 372, 71, 25);
		contentPane.add(btnComp10);
		btnComp10.setVisible(false);
	
	}
}
